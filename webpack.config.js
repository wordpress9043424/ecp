import { sync } from 'glob'
import { fileURLToPath } from 'url'
import { resolve, dirname,  } from 'path'
import TerserJSPlugin from 'terser-webpack-plugin'
import WebpackShellPluginNext from 'webpack-shell-plugin-next'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin'


const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)
const { loader: _loader } = MiniCssExtractPlugin
const jsOldFiles = sync('./widgets/**/*.js').map(f => './' + f)
const jsAdminFiles = sync('./admin/**/*.js').map(f => './' + f)
const jsFiles = sync('./components/**/*.js').map(f => './' + f)
const scssFiles = sync('./components/**/*.{scss,css,sass}').map(f => './' + f)
const scssOldFiles = sync('./widgets/**/*.{scss,css,sass}').map(f => './' + f)
const scssAdminFiles = sync('./admin/**/*.{scss,css,sass}').map(f => './' + f)

export default {
  entry: {
    ...jsFiles.reduce((entries, file) => {
      const name = 'js/' + file.split('/').pop().replace(/\.js$/, '');
      entries[name] = file;
      return entries;
    }, {}),
    ...scssFiles.reduce((entries, file) => {
      const name = 'css/' +  file.split('/').pop().replace(/\.(s?css|sass)$/, '');
      entries[name] = file;
      return entries;
    }, {}),
    ...jsOldFiles.reduce((entries, file) => {
      const name = 'js/' + file.split('/').pop().replace(/\.js$/, '');
      entries[name] = file;
      return entries;
    }, {}),
    ...scssOldFiles.reduce((entries, file) => {
      const name = 'css/' +  file.split('/').pop().replace(/\.(s?css|sass)$/, '');
      entries[name] = file;
      return entries;
    }, {}),
    ...jsAdminFiles.reduce((entries, file) => {
      const name = 'admin/js/' + file.split('/').pop().replace(/\.js$/, '');
      entries[name] = file;
      return entries;
    }, {}),
    ...scssAdminFiles.reduce((entries, file) => {
      const name = 'admin/css/' +  file.split('/').pop().replace(/\.(s?css|sass)$/, '');
      entries[name] = file;
      return entries;
    }, {}),
  },
  output: {
    filename: '[name].min.js',
    path: resolve(__dirname, 'assets'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.scss$/,
        use: [
          _loader,
          'css-loader',
          'sass-loader'
        ],
      },
    ],
  },
  optimization: {
    minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ['*', '!lib'],
    }),
    new MiniCssExtractPlugin({
      filename: '[name].min.css',
    }),
    new WebpackShellPluginNext({
      onBuildEnd: {
        scripts: [
          'rm ./assets/css/*.min.js',
          'rm ./assets/admin/css/*.min.js'
        ]
      }
    })
  ],
  resolve: {
    extensions: ['.js', '.scss'],
  },
};
