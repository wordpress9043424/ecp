jQuery(function ($) {
  console.log('box', $('#ecp__infinity'))
  console.log(document.querySelectorAll('#ecp__infinity'))
  $('[ecp__infinity]').each(function () {
    const box = $(this)
    const element = box.find('.ecp__overflow')
    const dragableElements =  box.find('img, a');
    let isDragging = false;
    let startX, scrollLeft;

    dragableElements.attr('draggable', 'false')
    const clean = () => {
      isDragging = false;
      element.removeClass('no-select');
    }

    const onDown = (pageX) => {
      isDragging = true;
      startX = pageX - element.offset().left;
      scrollLeft = element.scrollLeft();
      element.addClass('no-select');
    }

    const onMove = (pageX) => {
      if (!isDragging) return;
      element.scrollLeft(scrollLeft - (pageX - element.offset().left - startX) * 1.6);
    }

    box.on('mousedown', (e) => onDown(e.pageX));
    box.on('touchstart', (e) => onDown(e.originalEvent.touches[0].pageX));
    $(window).on('mousemove', (e) => onMove(e.pageX));
    $(window).on('touchmove', (e) => onMove(e.originalEvent.touches[0].pageX));
    $(window).on('mouseup', clean);
    $(window).on('touchend', clean);
    $(window).on('blur', clean);

    $(window).on('beforeunload', function () {
      box.off('mousedown');
      box.off('touchstart');
      $(window).off('mousemove');
      $(window).off('touchmove');
      $(window).off('mouseup');
      $(window).off('touchend');
      $(window).off('blur');
    });

  })

  $('.elementor-widget-text-editor').each(function() {
    let content = $(this).html().trim();
    let lastSpaceIndex = content.lastIndexOf(' ');

    if (lastSpaceIndex !== -1) {
      $(this).html(content.substring(0, lastSpaceIndex) + '&nbsp;' + content.substring(lastSpaceIndex + 1));
    }
  });
});
