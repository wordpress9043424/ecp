( function( $ ) {

	"use strict";

  const addCss = (css, context) => {
    if ( ! context ) return;

    const model = context.model
    const customCSS = model.get('settings').get('ecp_custom_css')
    const attributes = model.get('settings').get('ecp_custom_attributes')

    const selector = 'document' === model.get('elType')
      ? elementor.config.document.settings.cssWrapperSelector
      : '.elementor-element.elementor-element-' + model.get('id')
  
    if ( customCSS ) css += customCSS.replace(/selector/g, selector)
    if ( attributes ) {
      try {
        $(selector).attr(JSON.parse(attributes))
      } catch (e) {
        console.error('Error parsing custom attributes', e)
      }
    }
    return css;
  }

  const addPageCss = () => {
		const customCSS = elementor.settings.page.model.get('ecp_custom_css');
		if (customCSS) {
			customCSS = customCSS.replace(/selector/g, elementor.config.settings.page.cssWrapperSelector);
			elementor.settings.page.getControlsCSS().elements.$stylesheetElement.append(customCSS);
		}
	}

  elementor.hooks.addFilter( 'editor/style/styleText', addCss)
  elementor.on('preview:loaded', addPageCss)
}( jQuery ))