<?php
/*
 * Plugin Name: Elementor Components Pack
 * Description: The only plugin you need for Elementor page builder.
 * Author: Artur Pędziwiatr
 * Version: 0.0.1
 * License: MIT
 *
 * Text Domain: ecp
 */

if (!defined('ABSPATH'))
  exit();

final class ECP_PLUGIN
{
  const VERSION = '1.0.0';
  const MINIMUM_ELEMENTOR_VERSION = '3.21.0';
  const MINIMUM_PHP_VERSION = '7.0';
  private static $_instance = null;

  public static function instance(): ECP_PLUGIN|null
  {
    if (is_null(self::$_instance)) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }

  public function __construct()
  {
    $this->define_constants();
    $this->add_utils();

    add_action('plugins_loaded', [$this, 'init']);
    add_action('admin_menu', [$this, 'add_ecp_menu']);
    add_action('elementor/init', [$this, 'initElementor']);
    add_action('wp_enqueue_scripts', [$this, 'scripts_styles']);
    add_action('elementor/frontend/after_enqueue_scripts', [$this, 'preview_scripts']);
    add_action('elementor/editor/after_enqueue_scripts', [$this, 'enqueue_editor_scripts']);
  }

  public function init()
  {
    $this->include_extension();
  }

  public function initElementor()
  {
    if (!did_action('elementor/loaded')) {
      add_action('admin_notices', [$this, 'admin_notice_missing_main_plugin']);
      return;
    }

    if (!version_compare(ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=')) {
      add_action('admin_notices', [$this, 'admin_notice_minimum_elementor_version']);
      return;
    }

    if (!version_compare(PHP_VERSION, self::MINIMUM_PHP_VERSION, '>=')) {
      add_action('admin_notices', [$this, 'admin_notice_minimum_php_version']);
      return;
    }

    $this->define_category();
    add_action('elementor/widgets/widgets_registered', [$this, 'include_widgets']);
  }

  public function add_utils()
  {
    require_once PLUGIN_PATH . 'utils/global_functions.php';
    require_once PLUGIN_PATH . 'utils/ajax_handler.php';
    require_once PLUGIN_PATH . 'admin/mega-menu/admin-mega-menu.php';
  }

  public function include_widgets()
  {
    require_once PLUGIN_PATH . 'utils/control-type-enum.php';
    require_once PLUGIN_PATH . 'utils/base-class.php';
    require_once PLUGIN_PATH . 'utils/widget-base.php';

    require_once PLUGIN_PATH . 'components/mega-menu/mega-menu.php';
    require_once PLUGIN_PATH . 'components/posts/ECP_Posts.php';
    require_once PLUGIN_PATH . 'components/slider-card/slider-card.php';
    require_once PLUGIN_PATH . 'components/toggle/ECP_Toggle.php';
  }

  public function include_extension()
  {
    require_once PLUGIN_PATH . 'extensions/ecp_global_css.php';
    require_once PLUGIN_PATH . 'extensions/ecp_global_condition.php';
    require_once PLUGIN_PATH . 'extensions/ecp_global_attribute.php';
    require_once PLUGIN_PATH . 'extensions/ecp_global_style.php';
  }

  public function add_ecp_menu()
  {
    require_once PLUGIN_PATH . 'admin/custom_post_type.php';
  }

  public function define_constants()
  {
    define('PLUGIN_URL', trailingslashit(plugins_url('/', __FILE__)));
    define('PLUGIN_PATH', trailingslashit(plugin_dir_path(__FILE__)));
  }

  public function scripts_styles()
  {
    wp_register_script('swiper-js', PLUGIN_URL . 'assets/lib/swiper-bundle.min.js', [], self::VERSION, true);
    wp_register_script('mapbox-js', PLUGIN_URL . 'assets/lib/mapbox-gl', [], self::VERSION, true);

    wp_register_style('ecp-posts-css', PLUGIN_URL . 'assets/css/posts.min.css', [], self::VERSION, 'all');
    wp_register_script('ecp-posts-js', PLUGIN_URL . 'assets/js/posts.min.js', ['jquery'], self::VERSION, true);
    wp_localize_script('ecp-posts-js', 'ajax', ['url' => admin_url('admin-ajax.php')]);

    wp_register_style('ecp-mega-menu-css', PLUGIN_URL . 'assets/css/mega-menu.min.css', [], self::VERSION, 'all');
    wp_register_script('ecp-mega-menu-js', PLUGIN_URL . 'assets/js/mega-menu.min.js', ['jquery'], self::VERSION, true);

    wp_register_style('ecp-slider-card-css', PLUGIN_URL . 'assets/css/slider-card.min.css', [], self::VERSION, 'all');
    wp_register_script('ecp-slider-card-js', PLUGIN_URL . 'assets/js/slider-card.min.js', ['jquery', 'swiper-js'], self::VERSION, true);
    
    wp_register_style('ecp-toggle-css', PLUGIN_URL . 'assets/css/toggle.min.css', [], self::VERSION, 'all');
    wp_register_script('ecp-toggle-js', PLUGIN_URL . 'assets/js/toggle.min.js', ['jquery', 'swiper-js'], self::VERSION, true);

    wp_enqueue_style('global-css');
    wp_enqueue_script('global-js');
  }

  public function preview_scripts()
  {
    ?>
      <script>
        jQuery(document).ready(function ($) {
          var elementorFrontend = window.parent.elementorFrontend;
          if (!elementorFrontend.isEditMode()) return
          if (typeof elementorFrontend !== 'undefined') {
            elementorFrontend.hooks.addAction('frontend/element_ready/widget', function ($scope, $) {
              if ($scope.find('.ecp__container__slider-img').length) {
                $.getScript('<?php echo PLUGIN_URL . 'assets/js/slider-old.min.js'; ?>');
              }
              if ($scope.find('ecp__container__map').length) {
                $.getScript('<?php echo PLUGIN_URL . 'assets/js/map.min.js'; ?>');
              }
              if ($scope.find('.ecp__container__menu').length) {
                $.getScript('<?php echo PLUGIN_URL . 'assets/js/menu.min.js'; ?>');
              }
              if ($scope.find('.ecp__container__toggle').length) {
                $.getScript('<?php echo PLUGIN_URL . 'assets/js/collapsible.min.js'; ?>');
              }
              if ($scope.find('.ecp__container__toggle2').length) {
                $.getScript('<?php echo PLUGIN_URL . 'assets/js/toggle.min.js'; ?>');
              }
              if ($scope.find('.ecp__container__slider-header').length) {
                $.getScript('<?php echo PLUGIN_URL . 'assets/js/slider-header.min.js'; ?>');
              }
              if ($scope.find('.ecp__container__post').length) {
                $.getScript('<?php echo PLUGIN_URL . 'assets/js/posts-old.min.js'; ?>');
              }
              if ($scope.find('.ecp__container__mm').length) {
                $.getScript('<?php echo PLUGIN_URL . 'assets/js/mega-menu.min.js'; ?>');
              }
            });
          }
        });
      </script>
    <?php
  }

  public function enqueue_editor_scripts()
  {
    wp_enqueue_script('editor-support-js', PLUGIN_URL . 'assets/js/editor.min.js', ['jquery'], '', true);
  }

  public function define_category()
  {
    Elementor\Plugin::instance()->elements_manager->add_category(
      'ecp-category',
      [
        'title' => 'ECP'
      ],
    );
  }

  public function admin_notice_missing_main_plugin()
  {
    if (isset($_GET['activate']))
      unset($_GET['activate']);
    $message = sprintf(
      esc_html__('"%1$s" requires "%2$s" to be installed and activated', 'ecp-widget'),
      '<strong>' . esc_html__('ECP Widget', 'ecp-widget') . '</strong>',
      '<strong>' . esc_html__('Elementor', 'ecp-widget') . '</strong>'
    );

    printf('<div class="notice notice-warning is-dimissible"><p>%1$s</p></div>', $message);
  }

  public function admin_notice_minimum_elementor_version()
  {
    if (isset($_GET['activate']))
      unset($_GET['activate']);
    $message = sprintf(
      esc_html__('"%1$s" requires "%2$s" version %3$s or greater', 'ecp-widget'),
      '<strong>' . esc_html__('ECP Widget', 'ecp-widget') . '</strong>',
      '<strong>' . esc_html__('Elementor', 'ecp-widget') . '</strong>',
      self::MINIMUM_ELEMENTOR_VERSION
    );

    printf('<div class="notice notice-warning is-dimissible"><p>%1$s</p></div>', $message);
  }

  public function admin_notice_minimum_php_version()
  {
    if (isset($_GET['activate']))
      unset($_GET['activate']);
    $message = sprintf(
      esc_html__('"%1$s" requires "%2$s" version %3$s or greater', 'ecp-widget'),
      '<strong>' . esc_html__('ECP Widget', 'ecp-widget') . '</strong>',
      '<strong>' . esc_html__('PHP', 'ecp-widget') . '</strong>',
      self::MINIMUM_PHP_VERSION
    );

    printf('<div class="notice notice-warning is-dimissible"><p>%1$s</p></div>', $message);
  }

}

ECP_PLUGIN::instance();
