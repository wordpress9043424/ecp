<?php

class CustomPostType
{
  public function __construct()
  {
    $this->add_submenu();
    add_action('admin_post_save_custom_post_type', [$this, 'save_custom_post_type']);
    add_action('init', [$this, 'register_custom_post_types']);
  }

  public function add_submenu()
  {
    add_submenu_page(
      'options-general.php',
      'Ustawienia Paragrafów',
      'Add custom post type',
      'manage_options',
      'paragrafy-settings',
      [$this, 'render']
    );
  }

  public function render()
  {
    ?>
    <div class="wrap">
      <h1>Dodaj nowy typ wpisów</h1>
      <form method="post" action="<?php echo admin_url('admin-post.php'); ?>">
        <input type="hidden" name="action" value="save_custom_post_type">
        <table class="form-table" role="presentation">
          <tr>
            <th scope="row"><label for="post_type_name">Nazwa identyfikatora (slug)</label></th>
            <td><input name="post_type_name" type="text" id="post_type_name" class="regular-text" required /></td>
          </tr>
          <tr>
            <th scope="row"><label for="singular_name">Nazwa pojedyncza</label></th>
            <td><input name="singular_name" type="text" id="singular_name" class="regular-text" required /></td>
          </tr>
          <tr>
            <th scope="row"><label for="plural_name">Nazwa mnoga</label></th>
            <td><input name="plural_name" type="text" id="plural_name" class="regular-text" required /></td>
          </tr>
        </table>
        <?php submit_button('Dodaj nowy typ wpisów'); ?>
      </form>
      <h2>Dostępne typy wpisów</h2>
      <ul>
        <?php
        $custom_post_types = get_option('custom_post_types', []);
        foreach ($custom_post_types as $cpt) {
          echo '<li>' . esc_html($cpt['plural_name']) . '</li>';
        }
        ?>
      </ul>
    </div>
    <?php
  }

  public function save_custom_post_type()
  {
    if (!current_user_can('manage_options')) {
      return;
    }

    check_admin_referer('save_custom_post_type');

    $post_type_name = sanitize_text_field($_POST['post_type_name']);
    $singular_name = sanitize_text_field($_POST['singular_name']);
    $plural_name = sanitize_text_field($_POST['plural_name']);

    $custom_post_types = get_option('custom_post_types', []);
    $custom_post_types[] = [
      'post_type_name' => $post_type_name,
      'singular_name' => $singular_name,
      'plural_name' => $plural_name
    ];
    update_option('custom_post_types', $custom_post_types);

    wp_redirect(admin_url('options-general.php?page=paragrafy-settings'));
    exit;
  }

  public function register_custom_post_types()
  {
    $custom_post_types = get_option('custom_post_types', []);
    foreach ($custom_post_types as $cpt) {
      register_post_type($cpt['post_type_name'], [
        'labels' => [
          'name' => __($cpt['plural_name']),
          'singular_name' => __($cpt['singular_name']),
        ],
        'public' => true,
        'has_archive' => true,
        'show_in_menu' => true,
        'supports' => ['title', 'editor', 'thumbnail', 'elementor'],
        'rewrite' => ['slug' => sanitize_title($cpt['plural_name'])],
      ]);
    }
  }
}

new CustomPostType();
