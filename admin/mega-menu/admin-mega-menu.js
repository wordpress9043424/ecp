jQuery(function ($) {
	"use strict";
  const { settings, nonce } = window['ecpSettings'];

  const extractId = (elem) => elem.attr('id').replace('menu-item-', '')
  const extractDepth = (elem) =>
    elem.attr( 'class' ).match( /menu-item-depth-\d/ )[0]?.replace( 'menu-item-depth-', '' ) ?? 0

  const openDialog = (e) => {
    const element = $(e.target)
    const id = element.attr('data-id')
    const depth = element.attr('data-depth')
    

    const dialog = $('.ecp__container__mega-menu')
    const title = $('.ecp__container__mega-menu .header__title')
    const btnClose = $('.ecp__container__mega-menu .header__close')
    const btnElementor = $('.ecp__container__mega-menu .btn__elementor')
    const btnElementorClose = $('.ecp__elementor .btn__close')
    const btnSave = $('.ecp__container__mega-menu .dialog__footer button')
    const switcherEnable = $('.ecp__container__mega-menu #ecp__mm_enable')
    
    dialog.fadeIn();
    dialog.css('display', 'flex')


    const createNewTemplate = () => {
      try {
        $.ajax({
          type: 'POST',
          url: ajaxurl,
          data: {
            action: 'ecp_new_mm_template',
            nonce,
            item_id: id,
            item_depth: depth
          },
          success: ({ data }) => {
            const elementor = $('.ecp__elementor')
            elementor.fadeIn().css('display', 'flex')
            setTimeout(() => btnElementorClose.css('opacity', 1), 1000)
            if ( !$('.mm_iframe').find('iframe').length ) {
              $('.mm_iframe').append(
                '<iframe id="mm_iframe" src="'+ data?.link +'" width="100%" height="100%"></iframe>'
              );
            }
          }
        });
      } catch {
        console.error('Cannot create link for mega menu')
      }
    }

    const closeDialog = () => {
      dialog.fadeOut()
      btnSave.off('click')
      btnClose.off('click')
      btnElementor.off('click')
      btnElementorClose.off('click')
    }

    const getConfig = (id) => ({
      ...settings[id],
      enable: switcherEnable.is(':checked'),
    })

    const setConfigInDialog = (id) => {
      const config = settings[id];
      console.log(config)
      if (!$.isEmptyObject(config)) {
        switcherEnable.prop('checked', config.enable)
      }
    }

    const saveTemplate = () => {
      const config = getConfig(id);
      $.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {
            action: 'ecp_save_mm_template',
            nonce,
            item_id: id,
            item_depth: depth,
            item_settings: config
        },
        success: function( response ) {
          btnSave.text('Saved');
          setTimeout(() => btnSave.text('Save') && btnSave.blur(), 1000);
          settings[id] = config;
        }
    });
    }

    const closeElementor = () => {
      const elementor = $('.ecp__elementor')
      elementor.fadeOut(400, () => elementor.find('iframe').remove())
      btnElementorClose.css('opacity', 0)
    }

    title.text( `ECP Mega Menu - (${element.closest('li').find('.menu-item-title').text()})`)
    btnClose.on('click', closeDialog)
    btnSave.on('click', saveTemplate)
    btnElementor.on('click', createNewTemplate)
    btnElementorClose.on('click', closeElementor)
    setConfigInDialog(id)
  }


  $( '#menu-to-edit .menu-item' ).each((_i,elem) => {
    const depth = extractDepth($(elem))
      const div = $('<div></div>')
      div.addClass('ecp__btn__open')
      div.attr('data-id', extractId($(elem)))
      div.attr('data-depth', depth)
      div.text('MegaMenu')
      $(elem).find('.menu-item-bar').append(div);
      div.on('click', openDialog)
  });
});