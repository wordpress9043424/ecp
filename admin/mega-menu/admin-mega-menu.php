<?php

add_action('init', 'ecp_register_mm_pt', 999);
if ( !is_admin() ) return;

add_filter( 'option_elementor_cpt_support', 'ecp_add_support_for_mm' );
add_filter( 'default_option_elementor_cpt_support', 'ecp_add_support_for_mm' );
add_action( 'admin_footer', 'ecp_render_mm', 10 );
add_action( 'wp_ajax_ecp_new_mm_template', 'ecp_new_mm_template' );
add_action( 'wp_ajax_ecp_save_mm_template', 'ecp_save_mm_template' );
add_action( 'admin_enqueue_scripts', 'ecp_mm_enqueue_scripts' );

function ecp_add_support_for_mm( $value ) {
    if ( empty( $value ) ) {
        $value = [];
    }
    
    return array_merge( $value, ['ecp_mega_menu'] ); 
}

function ecp_render_mm() {
    $screen = get_current_screen();
    
    if ( 'nav-menus' !== $screen->base ) {
        return;
    }

    ?>
    <div class="ecp__container__mega-menu">
        <div class="dialog">
            <div class="dialog__header">
                <span class="header__title"><?php esc_html_e('ECP Mega Menu', 'ecp-widget'); ?></span>
                <span class="header__close"><i class="eicon-close btn__close" aria-hidden="true"></i></span>
            </div>

            <div class="dialog__content">
                <h4 class="content__title --line"><?php esc_html_e('General', 'ecp-widget'); ?></h4>

                <div class="content__elementor --line">
                    <h4><?php esc_html_e('Enable Mega Menu', 'ecp-widget'); ?></h4>
                    <label for="ecp__mm_enable" class="slider">
                        <input type="checkbox" id="ecp__mm_enable">
                        <span class="slider__round"></span>
                    </label>
                </div>

                <div class="content__elementor --line">
                    <h4><?php esc_html_e('Content', 'ecp-widget'); ?></h4>
                    <button class="btn__elementor --ecp-btn">
                        <i class="eicon-elementor-square" aria-hidden="true"></i>
                        <?php esc_html_e('Edit with Elementor', 'ecp-widget'); ?>
                    </button>
                </div>
            </div>

            <div class="dialog__footer">
                <button class="--ecp-btn"><?php esc_html_e('Save', 'ecp-widget'); ?></button>
            </div>
        </div>
    </div>

    <div class="ecp__elementor">
        <i class="eicon-close btn__close" aria-hidden="true"></i>
        <div class="mm_iframe"></div>
    </div>
    <?php
}

function ecp_register_mm_pt() {
    $args = [
        'label'				  => esc_html__( 'ECP Mega Menu', 'ecp-widget' ),
        'public'              => true,
        'publicly_queryable'  => true,
        'rewrite'             => false,
        'show_ui'             => true,
        'show_in_menu'        => false,
        'show_in_nav_menus'   => false,
        'exclude_from_search' => true,
        'capability_type'     => 'post',
        'supports'            => ['title', 'editor', 'elementor'],
        'hierarchical'        => false,
    ];

    register_post_type( 'ecp_mega_menu', $args );
    add_action( 'template_include', 'ecp_filter_custom_template', 999 );
}

function ecp_mm_enqueue_scripts( $hook ) {

    $version = ECP_PLUGIN::instance()::VERSION;
    if ( 'nav-menus.php' == $hook ) {
        wp_enqueue_script( 'ecp-iconpicker-js', PLUGIN_URL .'assets/lib/iconpicker.min.js', ['jquery'], $version, true );
        wp_enqueue_style( 'ecp-iconpicker-css', PLUGIN_URL .'assets/lib/iconpicker.min.css', [], $version, true );
        wp_enqueue_style( 'ecp-el-fontawesome-css', ELEMENTOR_URL .'assets/lib/font-awesome/css/all.min.css', [], $version );
        wp_enqueue_style( 'ecp-admin-mega-menu-css', PLUGIN_URL .'assets/admin/css/admin-mega-menu.min.css', [], $version );
        wp_enqueue_script( 'ecp-admin-mega-menu-js', PLUGIN_URL .'assets/admin/js/admin-mega-menu.min.js', ['jquery'], $version );

        wp_localize_script( 
            'ecp-admin-mega-menu-js',
            'ecpSettings',
            [
                'settings' => ecp_get_mm_items_settings(),
				'nonce' => wp_create_nonce( 'ecp-admin-mega-menu-js' ),
            ]
        );

    }

}

function ecp_get_selected_mm_id() {
    $nav_menus = wp_get_nav_menus( array('orderby' => 'name') );
    $menu_count = count( $nav_menus );
    $nav_menu_selected_id = isset( $_REQUEST['menu'] ) ? (int) $_REQUEST['menu'] : 0;
    $add_new_screen = ( isset( $_GET['menu'] ) && 0 == $_GET['menu'] ) ? true : false;

    $current_menu_id = $nav_menu_selected_id;

    $page_count = wp_count_posts( 'page' );
    $one_theme_location_no_menus = ( 1 == count( get_registered_nav_menus() ) && ! $add_new_screen && empty( $nav_menus ) && ! empty( $page_count->publish ) ) ? true : false;

    $recently_edited = absint( get_user_option( 'nav_menu_recently_edited' ) );
    if ( empty( $recently_edited ) && is_nav_menu( $current_menu_id ) ) {
        $recently_edited = $current_menu_id;
    }

    if ( empty( $current_menu_id ) && ! isset( $_GET['menu'] ) && is_nav_menu( $recently_edited ) ) {
        $current_menu_id = $recently_edited;
    }

    if ( ! $add_new_screen && 0 < $menu_count && isset( $_GET['action'] ) && 'delete' == $_GET['action'] ) {
        $current_menu_id = $nav_menus[0]->term_id;
    }

    if ( $one_theme_location_no_menus ) {
        $current_menu_id = 0;
    } elseif ( empty( $current_menu_id ) && ! empty( $nav_menus ) && ! $add_new_screen ) {
        $current_menu_id = $nav_menus[0]->term_id;
    }

    return $current_menu_id;
}

function ecp_get_mm_items_settings() {

    $menu_id = ecp_get_selected_mm_id();
    if ( !$menu_id ) return [];
    $menu = wp_get_nav_menu_object( $menu_id );
    $menu_items = wp_get_nav_menu_items( $menu );

    $settings = [];

    if ( !$menu_items ) return [];
    foreach ( $menu_items as $key => $item_object ) {
        $item_id = $item_object->ID;
        $item_meta = get_post_meta( $item_id, 'ecp_mm_settings', true );
        $settings[ $item_id ] = empty($item_meta) ? [] : $item_meta;
    }
    
    return $settings;
}

function ecp_filter_custom_template( $template ) {
    if ( is_singular('ecp_mega_menu') ) {
        return PLUGIN_PATH . 'admin/templates/ecp-canvas.php';
    }
    return $template;
}

function ecp_new_mm_template() {
    $nonce = $_POST['nonce'];
    if ( !wp_verify_nonce( $nonce, 'ecp-admin-mega-menu-js' )  || !current_user_can( 'manage_options' ) ) {
      return; 
    }

    $menu_item_id = intval( $_POST['item_id'] );
    $mega_menu_id = get_post_meta( $menu_item_id, 'ecp_mm_li', true );
    if ( ! $mega_menu_id ) {
        $mega_menu_id = wp_insert_post( array(
            'post_title'  => 'ecp_mm_title' . $menu_item_id,
            'post_status' => 'publish',
            'post_type'   => 'ecp_mega_menu',
        ) );

        update_post_meta( $menu_item_id, 'ecp_mm_li', $mega_menu_id );
    }

    $edit_link = add_query_arg(
        [
            'post' => $mega_menu_id,
            'action' => 'elementor',
        ],
        admin_url( 'post.php' )
    );

    wp_send_json([
        'data' => ['link' => $edit_link]
    ]);
}

function ecp_save_mm_template() {
    $nonce = $_POST['nonce'];
    if ( !wp_verify_nonce( $nonce, 'ecp-admin-mega-menu-js' )  || !current_user_can( 'manage_options' ) ) {
      exit;
    }
    if ( isset($_POST['item_settings']) ) {
        update_post_meta( $_POST['item_id'], 'ecp_mm_settings', $_POST['item_settings'] );
    }
    wp_send_json_success($_POST['item_settings']);
}