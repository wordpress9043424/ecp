<?php

use Elementor\Utils;

if (!defined('ABSPATH')) {
	exit;
}

\Elementor\Plugin::$instance->frontend->add_body_class('elementor-template-canvas');
$is_preview_mode = \Elementor\Plugin::$instance->preview->is_preview_mode();
$woocommerce_class =  $is_preview_mode && class_exists( 'WooCommerce' ) ? 'woocommerce woocommerce-page' : '';
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<?php if (!current_theme_supports('title-tag')): ?>
		<title><?php echo wp_get_document_title(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></title>
	<?php endif; ?>
	<?php wp_head(); ?>
	<?php

	Utils::print_unescaped_internal_string( Utils::get_meta_viewport( 'canvas' ) );
	?>
</head>

<body <?php body_class($woocommerce_class); ?>>
	<?php
	Elementor\Modules\PageTemplates\Module::body_open();
	if (\Elementor\Plugin::$instance->preview->is_preview_mode() || is_singular(post_types: 'ecp_mega_menu')) {
		\Elementor\Plugin::$instance->modules_manager->get_modules('page-templates')->print_content();
	}
	wp_footer();
	?>
</body>