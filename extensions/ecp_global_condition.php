<?php
use \Elementor\Controls_Manager;

if (!defined('ABSPATH') && defined('ELEMENTOR_PRO_VERSION'))
	exit;

class ECP_Advanced_Condition
{
	public function __construct()
	{
		add_action('elementor/element/after_section_end', [$this, 'register_controls'], 10,  2);
    add_action('elementor/frontend/before_render', [$this, 'apply_conditions']);

	}

	public function register_controls($widget, $section_id)
	{
		if ( 'section_custom_css_pro' !== $section_id ) {
			return;
		}
		
    $widget->start_controls_section(
			'ecp_advanced_custom_condition',
			[
				'label' => esc_html__('Conditions - ECP', 'ecp-widget'),
				'tab' => Controls_Manager::TAB_ADVANCED
			]
		);

    $widget->add_control(
      'ecp_hide_on_pages',
      [
        'label' => __('Nie wyświetlaj na stronach: ', 'ecp-widget'),
        'type' => Controls_Manager::TEXT,
        'description' => __('Wprowadź ID stron oddzielone przecinkami, na których sekcja ma nie być wyświetlana.', 'ecp-widget'),
      ]
    );

		$widget->add_control(
      'ecp_disable_on_404',
      [
        'label' => __('Wyłącz na stronie 404', 'ecp-widget'),
        'type' => Controls_Manager::SWITCHER,
        'description' => __('Wyłącz tę sekcję na stronie 404 Not Found.', 'ecp-widget'),
      ]
    );
    $widget->add_control(
      'ecp_only_on_404',
      [
        'label' => __('Visibility only on 404 pape', 'ecp-widget'),
        'type' => Controls_Manager::SWITCHER,
        'description' => __('Wyłącz tę sekcję na stronie 404 Not Found.', 'ecp-widget'),
      ]
    );

		$widget->end_controls_section();
	}

	public static function apply_conditions($widget)
  {
		$settings = $widget->get_settings_for_display();

    $not_display = array_map('trim', explode(',', $settings['ecp_hide_on_pages']));
    $current_page_id = get_the_ID();

    if (
			(is_404() && $settings['ecp_disable_on_404'] == 'yes') ||
			(!empty($settings['ecp_hide_on_pages']) && in_array($current_page_id, $not_display))
		 ) {
      $widget->add_render_attribute('_wrapper', 'style', 'display:none;');
    }
    if (!is_404() && $settings['ecp_only_on_404'] == 'yes') {
      $widget->add_render_attribute('_wrapper', 'style', 'display:none;');
    }
  }

}

new ECP_Advanced_Condition();
