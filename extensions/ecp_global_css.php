<?php
use \Elementor\Controls_Manager;

if (!defined('ABSPATH') && defined('ELEMENTOR_PRO_VERSION'))
	exit;

class ECP_Advanced_CSS
{
	public function __construct()
	{
		add_action('elementor/element/after_section_end', [$this, 'register_controls'], 25, 3);
    add_action('elementor/element/parse_css', [$this, 'appply_css'], 10, 2);
	}

	public function register_controls($widget, $section_id, $args)
	{
    if ( 'section_custom_css_pro' !== $section_id ) {
			return;
		}
		
    $widget->start_controls_section(
			'ecp_advanced_custom_css',
			[
				'label' => esc_html__('Custom CSS - ECP', 'ecp-widget'),
				'tab' => Controls_Manager::TAB_ADVANCED
			]
		);

		$widget->add_control(
			'ecp_custom_css',
			[
				'type' => Controls_Manager::CODE,
				'label' => esc_html__('Custom CSS', 'ecp-widget'),
				'language' => 'css',
				'render_type' => 'ui',
				'label_block' => true,
			]
		);
		$widget->end_controls_section();
	}

	public function appply_css($post_css, $element) 
	{
		$element_settings = $element->get_settings();
		if (empty($element_settings['ecp_custom_css'])) {
			return;
		}
		$css = trim($element_settings['ecp_custom_css']);
		if (empty($css)) {
			return;
		}
		$css = str_replace('selector', $post_css->get_element_unique_selector($element), $css);
		$css = sprintf('/* Start custom CSS for %s, class: %s */', $element->get_name(), $element->get_unique_selector()) . $css . '/* End custom CSS */';
		$post_css->get_stylesheet()->add_raw_css($css);
	}

}

new ECP_Advanced_CSS();
