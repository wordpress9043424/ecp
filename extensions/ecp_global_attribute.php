<?php
use \Elementor\Controls_Manager;

if (!defined('ABSPATH') && defined('ELEMENTOR_PRO_VERSION'))
	exit;

class ECP_Advanced_Attributes
{
	public function __construct()
	{
		add_action('elementor/element/before_section_start', [$this, 'register_controls'], 25, 3);
		add_action('elementor/frontend/before_render', [$this, 'appply_arguments']);
	}

	public function register_controls($widget, $section_id, $args)
	{
    if ( 'section_custom_attributes_pro' !== $section_id ) {
			return;
		}

    $widget->start_controls_section(
			'ecp_advanced_custom_attributes',
			[
				'label' => esc_html__('Custom Attributes - ECP', 'ecp-widget'),
				'tab' => Controls_Manager::TAB_ADVANCED
			]
		);

		$widget->add_control(
			'ecp_custom_attributes',
			[
				'label' => __('Custom Attributes', 'ecp-widget'),
				'type' => Controls_Manager::TEXTAREA,
				'description' => __('Add custom attributes for this section in JSON format', 'ecp-widget'),
			]
		);

		$widget->end_controls_section();
	}


	public function appply_arguments($widget)
	{
		if (!empty($widget->get_settings('ecp_custom_attributes'))) {
			$array = json_decode($widget->get_settings('ecp_custom_attributes'), true);
			if (json_last_error() === JSON_ERROR_NONE) {
				$widget->add_render_attribute(
					'_wrapper',
					$array
				);
			}
		}
	}
}

new ECP_Advanced_Attributes();
