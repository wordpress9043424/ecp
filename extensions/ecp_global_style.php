<?php
use \Elementor\Controls_Manager;

if (!defined('ABSPATH') && defined('ELEMENTOR_PRO_VERSION'))
	exit;

class ECP_Style_CustomStyle
{
	public function __construct()
	{
		add_action('elementor/element/after_section_end', [$this, 'register_controls'], 10,  2);
	}

	public function register_controls($widget, $section_id)
	{
		if ( 'section_custom_css_pro' !== $section_id ) {
			return;
		}

		### SIZE/POSITION ###
    $widget->start_controls_section(
			'ecp_style_custom_size',
			[
				'label' => esc_html__('Size/Position - ECP', 'ecp-widget'),
				'tab' => Controls_Manager::TAB_STYLE
			]
		);
    $widget->add_responsive_control(
			'ecp_global_flex',
			[
				'type' => Controls_Manager::TEXT,
				'label' => __('Flex', 'ecp-widget'),
				'selectors' => [
					'{{WRAPPER}}[data-element_type="container"]' => 'flex: {{VALUE}};',
				],
			]
		);
		$widget->add_responsive_control(
			'ecp_global_max_width',
			[
				'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Max Width', 'ecp-widget'),
				'size_units' => ['px', '%', 'vw'],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 2000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
					'vw' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => 'max-width: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);
		$widget->add_responsive_control(
			'ecp_global_min_width',
			[
				'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Min Width', 'ecp-widget'),
				'size_units' => ['px', '%', 'vw'],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 2000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
					'vw' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => 'min-width: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);
		$widget->add_responsive_control(
			'ecp_global_width',
			[
				'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Width', 'ecp-widget'),
				'size_units' => ['px', '%', 'vw'],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 2000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
					'vw' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => 'width: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);
		$widget->add_responsive_control(
			'ecp_global_max_height',
			[
				'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Max Height', 'ecp-widget'),
				'size_units' => ['px', '%', 'vh', 'custom'],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 2000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
					'vh' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => 'max-height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$widget->add_responsive_control(
			'ecp_global_min_height',
			[
				'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Min Height', 'ecp-widget'),
				'size_units' => ['px', '%', 'vh', 'custom'],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 2000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
					'vh' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => 'min-height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$widget->add_responsive_control(
			'ecp_global_height',
			[
				'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Height', 'ecp-widget'),
				'size_units' => ['px', '%', 'vh', 'custom'],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 2000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
					'vh' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$widget->add_control(
			'ecp_global_aspect_ratio',
			[
				'type' => Controls_Manager::TEXT,
				'label' => esc_html__('Aspect ratio', 'ecp-widget'),
				'description' => esc_html__('Enter aspect ratio in the format width/height, e.g., 16/9.', 'ecp-widget'),
				'selectors' => [
					'{{WRAPPER}} img' => 'aspect-ratio: {{VALUE}};',
				],
			]
		);
		$widget->end_controls_section();

		### FONT ###
		$widget->start_controls_section(
			'ecp_style_custom_font',
			[
				'label' => esc_html__('Font - ECP', 'ecp-widget'),
				'tab' => Controls_Manager::TAB_STYLE
			]
		);	
		$widget->add_responsive_control(
			'ecp_global_font_size',
			[
				'type' => Controls_Manager::SLIDER,
				'label' => __('Font size', 'ecp-widget'),
				'size_units' => ['px', '%', 'vh', 'custom'],
				'selectors' => [
					'{{WRAPPER}} .elementor-widget-container h1' => 'font-size: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .elementor-widget-container h2' => 'font-size: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .elementor-widget-container h3' => 'font-size: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .elementor-widget-container h4' => 'font-size: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .elementor-widget-container p' => 'font-size: {{SIZE}}{{UNIT}} !important;',
				],
				'devices' => ['desktop', 'tablet', 'mobile'],
			]
		);
		$widget->add_responsive_control(
			'ecp_global_line_height',
			[
				'type' => Controls_Manager::SLIDER,
				'label' => __('Line height', 'ecp-widget'),
				'size_units' => ['px', '%', 'vh', 'custom'],
				'selectors' => [
					'{{WRAPPER}} ' => 'line-height: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .elementor-widget-container h1' => 'line-height: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .elementor-widget-container h2' => 'line-height: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .elementor-widget-container h3' => 'line-height: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .elementor-widget-container h4' => 'line-height: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .elementor-widget-container p' => 'line-height: {{SIZE}}{{UNIT}} !important;',
				],
				'devices' => ['desktop', 'tablet', 'mobile'],
			]
		);
		$widget->add_responsive_control(
			'ecp_global_letter_spacing',

			[
				'type' => Controls_Manager::SLIDER,
				'label' => __('Letter spacing', 'ecp-widget'),
				'size_units' => ['px', '%', 'vh', 'custom'],
				'selectors' => [
					'{{WRAPPER}} ' => 'letter-spacing: {{SIZE}}{{UNIT}} !important;',
				],
				'devices' => ['desktop', 'tablet', 'mobile'],
			]
		);
		$widget->end_controls_section();

		### OVERFLOW ###
		$widget->start_controls_section(
			'ecp_style_custom_overflow',
			[
				'label' => esc_html__('Overflow - ECP', 'ecp-widget'),
				'tab' => Controls_Manager::TAB_STYLE
			]
		);	
		$widget->add_responsive_control(
			'ecp_global_overflow_x',
			[
				'type' => Controls_Manager::SELECT,
				'label' => __('Overflow X', 'ecp-widget'),
				'options' => [
					'visible' => __('Visible', 'ecp-widget'),
					'clip' => __('Clip', 'ecp-widget'),
					'hidden' => __('Hidden', 'ecp-widget'),
					'scroll' => __('Scroll', 'ecp-widget'),
					'auto' => __('Auto', 'ecp-widget'),
					'initial' => __('Initial', 'ecp-widget'),
					'inherit' => __('Inherit', 'ecp-widget'),
				],
				'selectors' => [
					'{{WRAPPER}}' => 'overflow-x: {{VALUE}} !important;',
				],
			]
		);
		$widget->add_responsive_control(
			'ecp_global_overflow_y',
			[
				'type' => Controls_Manager::SELECT,
				'label' => __('Overflow Y', 'ecp-widget'),
				'options' => [
					'visible' => __('Visible', 'ecp-widget'),
					'clip' => __('Clip', 'ecp-widget'),
					'hidden' => __('Hidden', 'ecp-widget'),
					'scroll' => __('Scroll', 'ecp-widget'),
					'auto' => __('Auto', 'ecp-widget'),
					'initial' => __('Initial', 'ecp-widget'),
					'inherit' => __('Inherit', 'ecp-widget'),
				],
				'selectors' => [
					'{{WRAPPER}}' => 'overflow-y: {{VALUE}} !important;',
				],
			]
		);
		$widget->end_controls_section();

		$widget->start_controls_section(
			'ecp_style_custom_icon',
			[
				'label' => esc_html__('Icon - ECP', 'ecp-widget'),
				'tab' => Controls_Manager::TAB_STYLE
			]
		);	
		$widget->add_responsive_control(
			'ecp_global_icon_size',
			[
				'label' => __('Icon size', 'ecp-widget'),
				'type' => Controls_Manager::SLIDER,
				'size_units' => ['px', '%', 'em'],
				'selectors' => [
					'{{WRAPPER}} ' => 'width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} svg[aria-hidden="true"]' => 'width: {{SIZE}}{{UNIT}};',
				],

			]
		);
		$widget->add_responsive_control(
			'ecp_global_icon_fill',
			[
				'label' => __('Icon fill', 'ecp-widget'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} ' => 'fill: {{VALUE}};',
					'{{WRAPPER}} svg' => 'fill: {{VALUE}};',
					'{{WRAPPER}} svg path' => 'fill: {{VALUE}};',
				],

			]
		);
		$widget->add_responsive_control(
			'ecp_global_icon_stroke',
			[
				'label' => __('Icon stroke', 'ecp-widget'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} ' => 'stroke: {{VALUE}};',
					'{{WRAPPER}}  svg' => 'stroke: {{VALUE}};',
				],

			]
		);
		$widget->add_responsive_control(
			'ecp_global_icon_stroke_width',
			[
				'label' => __('Icon stroke width', 'ecp-widget'),
				'type' => Controls_Manager::SLIDER,
				'size_units' => ['px', '%', 'em'],
				'selectors' => [
					'{{WRAPPER}} ' => 'stroke-width: {{VALUE}};',
					'{{WRAPPER}}  svg' => 'stroke-width: {{VALUE}};',
				],

			]
		);
		$widget->end_controls_section();
	}
}

new ECP_Style_CustomStyle();
