<?php

class ECP_Mega_Menu_Mobile_Walker extends \ECP_Base_Walker {
  public function start_lvl( &$output, $depth = 0, $args = [] ) {
		if ( 'mega-menu' === $this->get_mm_elem_type() ) return;

		if ( isset($args->item_spacing) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = str_repeat($t, $depth);
		$class_names = join(' ', apply_filters( 'nav_menu_submenu_css_class', ['sub-menu', 'ecp__submenu'], $args, $depth ));
		$class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

		$output .= "{$n}{$indent}<ul $class_names>{$n}";
	}

  public function end_lvl( &$output, $depth = 0, $args = array() ) {
		if ( 'mega-menu' === $this->get_mm_elem_type() ) return;

		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = str_repeat( $t, $depth );
		$output .= "$indent</ul>{$n}";
	}

  public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$this->set_mm_elem_type( $item->ID, $depth );
		
		if ( 'mega-menu' === $this->get_mm_elem_type() ) return;
    $settings = $this->get_mm_config($item->ID);

		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}

		$indent   = $depth ? str_repeat( $t, $depth ) : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$classes   = empty( $item->classes ) ? [] : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		if ( $this->is_mm_enable( $item->ID ) ) {
			$classes[] = 'menu-item-has-children';
		}

		$id_attr = '';
		$custom_width_attr = '';

		if ( $settings && $settings['enable'] ) {
      $id_attr = ' data-id="'. $item->ID .'"';
      $classes[] = 'ecp__mm__enable';
		}

		$li_class = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$li_class = $li_class ? ' class="' . esc_attr( $li_class ) . '"' : '';

    $a_class = 'ecp__menu__mobile-link';

		if ( in_array( 'current-menu-item', $item->classes ) ) {
			$a_class .= ' --active';
		}
		
		$output .= $indent .'<li'. $id . $li_class . $custom_width_attr . $id_attr .'>';

    $item_output = $args->before;
    if ( in_array( 'menu-item-has-children', $item->classes ) || $this->is_mm_enable( $item->ID ) ) {
      if ( in_array( 'current-menu-item', $item->classes ) || in_array( 'current-menu-ancestor', $item->classes ) ) {
        $a_class .= ' --active';
      }
      $submenu_icon = $depth > 0
        ? '<i class="wpr-sub-icon fas wpr-sub-icon-rotate" aria-hidden="true"></i>'
        : '<i class="wpr-sub-icon fas" aria-hidden="true"></i>';
    }

    if ( $depth > 0 ) {
      $a_class .= ' ecp__menu__mobile__subitem';

      if ( in_array( 'current-menu-item', $item->classes ) || in_array( 'current-menu-ancestor', $item->classes ) ) {
        $a_class .= ' --active';
      }
    }
           
    $atts = [
      'title' => !empty( $item->attr_title ) ? $item->attr_title : '',
      'target' => !empty( $item->target ) ? $item->target : '',
      'rel' => !empty( $item->xfn ) ? $item->xfn : '',
      'href' => !empty( $item->url ) ? $item->url : '#',
      'class' => $a_class,
    ];
		if ( $settings && $settings['enable'] ) {
			$atts['href'] = '#';
		}

    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

    $attributes = '';

    foreach ( $atts as $attr => $value ) {
      if ( !empty( $value ) ) {
        $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';
      }
    }
                
    $item_output .= '<a'. $attributes .'>';
      $item_output .= $args->link_before;
      $item_output .= '<span>'. apply_filters( 'the_title', $item->title, $item->ID ) .'</span>';
      $item_output .= $args->link_after;
      $item_output .= $submenu_icon;
    $item_output .= '</a>';
    $item_output .= $args->after;
		
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

  public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		if ( 'mega-menu' === $this->get_mm_elem_type() ) return;
		
		if ( $depth === 0 ) {
			if ( $this->is_mm_enable($item->ID) ) {
				$elementor = \Elementor\Plugin::instance();
				$content = $elementor->frontend->get_builder_content_for_display(
            get_post_meta( $item->ID, 'ecp_mm_li', true)
          );
				$output .= '<div class="ecp__mm__mobile__content">'. $content .'</div>';
			}

			$output .= "</li>\n";
		}
	}
}