<?php

class ECP_Base_Walker extends \Walker_Nav_Menu {
  public function set_mm_elem_type( $id = 0, $depth = 0 ) {
		if ( 0 < $depth ) return;
		wp_cache_set(
      'mm_elem_type',
      $this->is_mm_enable($id) ? 'mega_menu' : 'normal_menu',
      'ecp-mega-menu'
    );
	}

	public function get_mm_elem_type() {
		return wp_cache_get( 'mm_elem_type', 'ecp-mega-menu' );
	}

  public function is_mega_menu($id, $depth) {
    return $depth == 0 && $this->is_mm_enable($id);
  }

	public function is_mm_enable($id) {
		$settings = $this->get_mm_config($id);
    if (!$settings) return false;
		return isset($settings['enable']);
	}

	public function get_mm_config($id) {
		$settings = get_post_meta( $id, 'ecp_mm_settings', true);
		return !empty($settings) ? $settings : false;
	}
}