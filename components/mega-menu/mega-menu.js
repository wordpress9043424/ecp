jQuery(function ($) {
  $('.ecp__container__mm').each(function () {
    const megaMenu = $(this);
    const id = megaMenu.attr('data-id');
    const desktopMenu = megaMenu.find('.ecp__nav');
    const mobileMenu = megaMenu.find('.ecp__nav__mobile');
    const mobileNav = megaMenu.find('.ecp__nav__mobile .ecp__nav__list');
    const subIcon = megaMenu.find('#ecp_submenu_icon .ecp__mm__submenu__icon');
    const burger =  megaMenu.find('.ecp__burger__checkbox');
    const desktopA = megaMenu.find('.ecp__mega__menu li  a')
    const megaMenuItems = megaMenu.find('.ecp__mm__enable');
    let windowHeight = $(window).height();

    const isInViewport = (element) => {
      const rect = element[0].getBoundingClientRect()
      const elemMid = (rect.top + rect.bottom) / 2
      return elemMid >= 0 && elemMid <= windowHeight
    }
  
    const updateActiveLink = () => {
      let currentActive = null;
      $('section').each(function() {
        if (isInViewport($(this))) {
          currentActive = $(this)
          return false
        }
      });
  
      if (currentActive) {
        $('.ecp__mega__menu li').removeClass('ecp__scroll__active')
        $(`nav a[href="#${currentActive.attr('id')}"]`).parent().addClass('ecp__scroll__active')
      }
    }
    updateActiveLink()

    const settings = window[id]
    if (subIcon.length > 0) {
      megaMenu.find('.menu-item-has-children').each((_i, li) => {
        const a = $(li).find('> a');
        a.find('> i').remove();
        a.append(subIcon.clone());
      })
      subIcon.remove();
    }

    desktopA.each((_i, e) => {
      const el = $(e)
      el.css('width', `${el.innerWidth() + 7}px`)
    })

    megaMenuItems.each((_i, elem) => {
      const content = $(elem).find('.ecp__mm__content');
      $(elem).hover(() => content.slideDown(300), () => content.slideUp(300));
    })

    mobileNav.find('.ecp__mm__enable').each((_i, elem) => {
      const content = $(elem).find('.ecp__mm__mobile__content');
      $(elem).on('click', () => {
        content.slideToggle(600);
      })
    })

    const swapMenu = () => {
      windowHeight = $(window).height();
      if (window.innerWidth < settings?.mobileActive) {
        desktopMenu.css('display', 'none');
        mobileMenu.css('display', 'flex');
        return;
      }
      desktopMenu.css('display', 'flex');
      mobileMenu.css('display', 'none');
    }

    const openMobileNav = () => {
      mobileNav.slideToggle(600);
      setTimeout(() => mobileNav.toggleClass('d-flex'), 300);
      mobileNav.toggleClass('ecp__menu__open');
      $('body').css('overflow', mobileNav.hasClass('ecp__menu__open') ? 'hidden' : 'auto');
    }

    burger.on('change', openMobileNav);
    swapMenu();
    $(window).on('resize', swapMenu);
    $(window).on('scroll', updateActiveLink)

    $(window).on('beforeunload', () => {
      burger.off('change');
      $(window).off('resize', swapMenu);
      $(window).off('scroll', updateActiveLink)
    });
  });
});
