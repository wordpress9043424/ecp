<?php

namespace ECP\Components;

use Elementor\Plugin;
use ECP\Enum\Control_Type;
use Elementor\Icons_Manager;
use ECP\Utils\ECP_Widget_Base;

if (!defined('ABSPATH')) exit;

class ECP_Mega_Menu extends ECP_Widget_Base
{
  protected $nav_menu_index = 1;

  public function get_name()
  {
    return 'ecp_mega_menu';
  }

  public function get_title()
  {
    return esc_html__('Mega menu', 'ecp-widget');
  }

  public function get_icon()
  {
    return 'eicon-nav-menu';
  }

  public function get_categories()
  {
    return ['ecp-category'];
  }

  public function get_style_depends()
  {
    return ['ecp-mega-menu-css'];
  }

  public function get_script_depends()
  {
    return ['ecp-mega-menu-js'];
  }

  protected function get_nav_menu_index()
  {
    return $this->nav_menu_index++;
  }

  private function get_available_menus()
  {
    $menus = wp_get_nav_menus();

    $options = ['' => 'Select a menu'];

    foreach ($menus as $menu) {
      $options[$menu->slug] = $menu->name;
    }

    return $options;
  }

  public function load_walkers()
  {
    require_once PLUGIN_PATH . 'components/mega-menu/walker/base-walker.php';
    require_once PLUGIN_PATH . 'components/mega-menu/walker/ecp-mm-walker.php';
    require_once PLUGIN_PATH . 'components/mega-menu/walker/ecp-mm-walker-mobile.php';
  }

  protected function render()
  {
    if (!$this->get_available_menus()) return;
    $this->load_walkers();

    $settings = $this->get_active_settings();

    $main_args = [
      'echo' => false,
      'menu' => $settings['ecp_mm_global_menu'],
      'menu_class' => 'ecp__mega__menu',
      'menu_id' => 'menu-' . $this->get_nav_menu_index() . '-' . $this->get_id(),
      'container' => '',
      'fallback_cb' => '__return_empty_string',
      'walker' => new \ECP_Mega_Menu_Walker(),
    ];

    add_filter('nav_menu_item_id', '__return_empty_string');
    $menu_html = wp_nav_menu($main_args);

    $mobile_args = [
      'echo' => false,
      'menu' => $settings['ecp_mm_global_menu'],
      'menu_class' => 'ecp__mega_menu_mobile',
      'menu_id' => 'mobile-menu-' . $this->get_nav_menu_index() . '-' . $this->get_id(),
      'container' => '',
      'fallback_cb' => '__return_empty_string',
      'walker' => new \ECP_Mega_Menu_Mobile_Walker(),
    ];

    $moible_menu_html = wp_nav_menu($mobile_args);
    remove_filter('nav_menu_item_id', '__return_empty_string');

    if (empty($menu_html)) {
      return;
    }

    $this->addJsConfig('ecp-mega-menu-js', [
      'mobileActive' => $settings['ecp_mm_mobile_active']
    ]);

    $template = $settings['ecp_mm_global_template'];
    ?>
      <div data-id="<?php echo $this->getId(); ?>" class="ecp__container__mm">
          <nav class="ecp__nav">
            <?php
            if (!empty($settings['ecp_mm_global_logo']['value'])):
              echo '<a class="ecp__nav__logo" href="/">';
              Icons_Manager::render_icon($settings['ecp_mm_global_logo'], ['aria-hidden' => 'true']);
              echo '</a>';
            endif;
            ?>

              <div class="ecp__nav__list">
                <?php
                echo $menu_html; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                if (!empty($template)) {
                  echo '<div class="ecp__nav__additional">';
                  echo Plugin::instance()->frontend->get_builder_content($template, true);
                  echo '</div>';
                }
                ?>
              </div>
          </nav>

          <nav class="ecp__nav__mobile">
              <div class="ecp__nav__mobile__head">
                <?php
                if (!empty($settings['ecp_mm_global_logo']['value'])):
                  echo '<a class="ecp__nav__logo" href="/">';
                  Icons_Manager::render_icon($settings['ecp_mm_global_logo'], ['aria-hidden' => 'true']);
                  echo '</a>';
                endif;

                $this->render_burger($settings);
                ?>
              </div>

              <div class="ecp__nav__list">
                <?php
                echo '' . $moible_menu_html; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                if (!empty($template)) {
                  echo '<div class="ecp__nav__additional">';
                  echo Plugin::instance()->frontend->get_builder_content($template, true);
                  echo '</div>';
                }
                ?>
              </div>

          </nav>
          <div id="ecp_submenu_icon">
            <?php renderIcon($settings['ecp_mm_global_icon_submenu'], 'ecp__mm__submenu__icon'); ?>
          </div>
      </div>
    <?php
  }

  protected function render_burger($settings)
  {
    ?>
      <div class="ecp__burger__checkbox">
          <label editor>
              <input type="checkbox">
              <svg viewBox="30 30 100 100" xmlns="http://www.w3.org/2000/svg">
                  <path class="line--1" d="M0 40h62c13 0 6 28-4 18L35 35"/>
                  <path class="line--2" d="M0 50h70"/>
                  <path class="line--3" d="M0 60h62c13 0 6-28-4-18L35 65"/>
              </svg>
          </label>
      </div>
    <?php
  }

  protected function content_controls()
  {
    return [
      [
        'id' => 'mm_global',
        'label' => 'Global',
        'controls' => [
          [
            'id' => 'mm_global_menu',
            'label' => 'Menu',
            'type' => Control_Type::SELECT,
            'options' => $this->get_available_menus(),
            'default' => '',
          ],
          [
            'id' => 'mm_global_logo',
            'label' => 'Logo',
            'type' => Control_Type::ICON,
          ],
          [
            'id' => 'mm_global_template',
            'label' => 'Template',
            'type' => Control_Type::TEMPLATE,
          ],
          [
            'id' => 'mm_global_icon_submenu',
            'label' => 'Icon for submenu',
            'type' => Control_Type::ICON,
          ],
        ]
      ],
      [
        'id' => 'mobile',
        'label' => 'Mobile',
        'controls' => [
          [
            'id' => 'mm_mobile_active',
            'label' => 'Active from',
            'type' => Control_Type::NUMBER,
          ],
        ]
      ],
    ];
  }

  protected function style_controls()
  {
    return [
      [
        'id' => 'mm_nav',
        'label' => 'Nav',
        'controls' => [
          [
            'id' => 'mm_nav_tab',
            'type' => Control_Type::TABS,
            'tabs' => [
              [
                'id' => 'mm_nav_tab_desktop',
                'label' => 'Desktop',
                'class' => '.ecp__container__mm .ecp__nav',
              ],
              [
                'id' => 'mm_nav_tab_mobile',
                'label' => 'Mobile',
                'class' => '.ecp__container__mm .ecp__nav__mobile',
              ],
            ],
            'controls' => [
              ['id' => 'spacing', 'type' => Control_Type::SPACING],
              ['id' => 'flexbox', 'type' => Control_Type::FLEXBOX],
              ['id' => 'border', 'type' => Control_Type::BORDER],
            ]
          ]
        ]
      ],
      [
        'id' => 'mm_nav_list',
        'label' => 'Nav list',
        'controls' => [
          [
            'id' => 'mm_nav_list_tab',
            'type' => Control_Type::TABS,
            'tabs' => [
              [
                'id' => 'mm_nav_list_tab_desktop',
                'label' => 'Desktop',
                'class' => '.ecp__container__mm .ecp__nav .ecp__nav__list .ecp__mega__menu',
              ],
              [
                'id' => 'mm_nav_list_tab_mobile',
                'label' => 'Mobile',
                'class' => '.ecp__container__mm .ecp__nav__mobile .ecp__nav__list .ecp__mega_menu_mobile',
              ],
            ],
            'controls' => [
              ['id' => 'spacing', 'type' => Control_Type::SPACING],
              ['id' => 'flexbox', 'type' => Control_Type::FLEXBOX],
              ['id' => 'border', 'type' => Control_Type::BORDER],
            ]
          ]
        ]
      ],
      [
        'id' => 'mm_nav_link',
        'label' => 'Nav link',
        'controls' => [
          [
            'id' => 'mm_nav_link_tab',
            'type' => Control_Type::TABS,
            'tabs' => [
              [
                'id' => 'mm_nav_link_tab_inactive',
                'label' => 'Inactive',
                'class' => '.ecp__container__mm .ecp__nav__list .ecp__menu-link',
              ],
              [
                'id' => 'mm_nav_link_tab_hover',
                'label' => 'Hover',
                'class' => '.ecp__container__mm .ecp__nav__list .ecp__menu-link:hover',
              ],
              [
                'id' => 'mm_nav_link_tab_active',
                'label' => 'Active',
                'class' => '.ecp__container__mm .ecp__nav__list .ecp__menu-link.--active',
              ],
            ],
            'controls' => [
              ['id' => 'spacing', 'type' => Control_Type::SPACING],
              ['id' => 'typography', 'type' => Control_Type::TYPOGRAPHY],
              ['id' => 'border', 'type' => Control_Type::BORDER],
            ]
          ]
        ]
      ],
      [
        'id' => 'mm_nav_link_mobile',
        'label' => 'Nav link mobile',
        'controls' => [
          [
            'id' => 'mm_nav_link_mobile_tab',
            'type' => Control_Type::TABS,
            'tabs' => [
              [
                'id' => 'mm_nav_link_mobile_tab_inactive',
                'label' => 'Inactive',
                'class' => '.ecp__container__mm .ecp__nav__list .ecp__menu__mobile-link',
              ],
              [
                'id' => 'mm_nav_link_mobile_tab_active',
                'label' => 'Active',
                'class' => '.ecp__container__mm .ecp__nav__list .ecp__menu__mobile-link.--active',
              ],
            ],
            'controls' => [
              ['id' => 'spacing', 'type' => Control_Type::SPACING],
              ['id' => 'typography', 'type' => Control_Type::TYPOGRAPHY],
              ['id' => 'border', 'type' => Control_Type::BORDER],
            ]
          ]
        ]
      ],
      [
        'id' => 'mm_nav_template',
        'label' => 'Nav template',
        'controls' => [
          [
            'id' => 'mm_nav_template_tab',
            'type' => Control_Type::TABS,
            'tabs' => [
              [
                'id' => 'mm_nav_template_tab_desktop',
                'label' => 'Desktop',
                'class' => '.ecp__container__mm .ecp__nav .ecp__nav__list .ecp__nav__additional',
              ],
              [
                'id' => 'mm_nav_template_tab_mobile',
                'label' => 'Mobile',
                'class' => '.ecp__container__mm .ecp__nav__mobile .ecp__nav__list .ecp__nav__additional',
              ],
            ],
            'controls' => [
              ['id' => 'spacing', 'type' => Control_Type::SPACING],
              ['id' => 'border', 'type' => Control_Type::BORDER],
            ]
          ]
        ]
      ],
      [
        'id' => 'mm_nav_subicon',
        'label' => 'Icon for submenu',
        'controls' => [
          [
            'id' => 'mm_nav_subicon_spacing',
            'type' => Control_Type::SPACING,
            'class' => '.ecp__container__mm .ecp__mm__submenu__icon',
          ],
        ]
      ],
      [
        'id' => 'mm_nav_subcontent',
        'label' => 'Mega menu content',
        'controls' => [
          [
            'id' => 'mm_nav_subcontent_spacing',
            'type' => Control_Type::SPACING,
            'class' => '.ecp__container__mm .ecp__mm__content',
          ],
          [
            'id' => 'mm_nav_subcontent_background',
            'type' => Control_Type::BACKGROUND,
            'class' => '.ecp__container__mm .ecp__mm__content',
          ],
        ]
      ],
      [
        'id' => 'mm_nav_subcontent_mobile',
        'label' => 'Mega menu mobile content',
        'controls' => [
          [
            'id' => 'mm_nav_subcontent_mobile_spacing',
            'type' => Control_Type::SPACING,
            'class' => '.ecp__container__mm .ecp__mm__content',
          ],
          [
            'id' => 'mm_nav_subcontent_mobile_background',
            'type' => Control_Type::BACKGROUND,
            'class' => '.ecp__container__mm .ecp__mm__content',
          ],
        ]
      ],
    ];
  }
}

Plugin::instance()->widgets_manager->register(new ECP_Mega_Menu());
