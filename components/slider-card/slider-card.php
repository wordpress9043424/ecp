<?php
namespace ECP\Components;

use Elementor\Plugin;
use ECP\Enum\Control_Type;
use Elementor\Icons_Manager;
use ECP\Utils\ECP_Widget_Base;
use Elementor\Group_Control_Image_Size;

if ( ! defined( 'ABSPATH' ) ) exit;

class ECP_Slider_Card extends ECP_Widget_Base
{

	public function get_name()
	{
		return 'ecp_slider_card';
	}

	public function get_title()
	{
		return esc_html__('Slider card', 'ecp-widget');
	}

	public function get_icon()
	{
		return 'eicon-media-carousel';
	}

	public function get_categories()
	{
		return ['ecp-category'];
	}

	public function get_style_depends()
	{
		return ['ecp-slider-card-css'];
	}

	public function get_script_depends()
	{
		return ['ecp-slider-card-js'];
	}

	protected function render() {
		$settings = $this->get_active_settings();
		$config = json_encode([
			'navigationCard' => $settings['ecp_sc_global_actions_position'],
			'slideEffect' => $settings['ecp_sc_global_slide_effect'],
		]);

		?>
		<div id="ecp_<?php echo $this->get_id(); ?>" class="ecp__container__sc" config='<?php echo $config; ?>'>
			<div class="ecp__container swiper">
				<div class="swiper-wrapper">
					<?php
					if($settings['ecp_sc_global_first_empty'])
						echo '<div class="ecp__container__card swiper-slide --empty"></div>';
					foreach ($settings['ecp_sc_content_cards'] as $card):
						echo '<div class="ecp__container__card swiper-slide">';
							$image_src = Group_Control_Image_Size::get_attachment_image_src($card['ecp_sc_content_card_image_bg']['id'], 'back_image_size', $card);
							if ($image_src) {
								$image_src = $card['ecp_sc_content_card_image_bg']['url'];
								echo '<img class="ecp__slide__img" alt="' . esc_attr($image_src) . '" src="' . esc_url($image_src) . '"/>';
							}
							echo '<div class="ecp__slide__card__helper">';
								echo '<div class="ecp__slide__card__inner">';
									$this->render_card($card);
								echo '</div>';	
							echo '</div>';	
						echo '</div>';
					endforeach;
					if($settings['ecp_sc_global_last_empty'])
						echo '<div class="ecp__container__card swiper-slide --empty"></div>';
					?>
				</div>
				<?php $this->render_actions($settings); ?>
			</div>
		</div>
		<?php
	}

	private function render_card($card) {
        renderIcon($card['ecp_sc_content_card_icon'], 'ecp__card__icon');
		echo '<div class="ecp__card__title">';
		echo '<h3>' . $card['ecp_sc_content_card_title'] . '</h3>';
		echo '</div>';
		
		if (!empty($card['ecp_sc_content_card_image']['url'])) {
			echo '<img class="ecp__card__image" src="' . $card['ecp_sc_content_card_image']['url'] . '" alt="' . $card['ecp_sc_content_card_image']['url'] . '">';
		}
		
		$template = $card['ecp_sc_content_card_template'];
		if (!empty($template)) {
			echo '<div class="ecp__card__template">';
			echo Plugin::instance()->frontend->get_builder_content($template, true);
			echo '</div>';
		}

		echo '<div class="ecp__card__second__title">';
		echo '<h4>' . $card['ecp_sc_content_card_second_title'] . '</h3>';
		echo '</div>';

		echo '<div class="ecp__card__copy">';
		echo $card['ecp_sc_content_card_copy'];
		echo '</div>';

		echo '<div class="ecp__card__actions">';
		echo '<a class="ecp__card__btn ' . $card['ecp_sc_content_card_button_class'] . '"' .
			get_url_link_string($card['ecp_sc_content_card_button_url']) .'>' .
			$card['ecp_sc_content_card_button'] . '</a>';
		echo '<div navigation ></div>';
		echo '</div>';
	}

	private function render_actions($settings) {
		echo '<div class="ecp__container__actions">';
		if ($settings['ecp_sc_global_show_btn_slider']) {
			echo '<div class="ecp__actions__btn --left"><i class="fas fa-chevron-left"></i></div>';
			echo '<div class="ecp__actions__btn --right"><i class="fas fa-chevron-right"></i></div>';
		}
		if ($settings['ecp_sc_global_show_padding']) {
			echo '<div class="ecp__pagination swiper-pagination"></div>';
		}
		echo '</div>';
	}

	protected function content_controls() {
		return [
			[
				'id' => 'sc_global',
				'label' => 'Global',
				'controls' => [
					[
						'id' => 'sc_global_first_empty',
						'label' => 'Add firts empty card',
						'type' => Control_Type::SWITCHER,
					],
					[
						'id' => 'sc_global_last_empty',
						'label' => 'Add last empty card',
						'type' => Control_Type::SWITCHER,
					],
					[
						'id' => 'sc_global_actions_position',
						'label' => 'Actions position',
						'type' => Control_Type::SELECT,
						'options' => [
							'inner_card' => 'Inner card',
							'outer_card' => 'Outer card',
						],
						'default' => 'inner_card',
					],
					[
						'id' => 'sc_global_show_padding',
						'label' => 'Show padding',
						'type' => Control_Type::SWITCHER,
					],
					[
						'id' => 'sc_global_show_btn_slider',
						'label' => 'Show button slider',
						'type' => Control_Type::SWITCHER,
					],
					[
						'id' => 'sc_global_slide_effect',
						'label' => 'Slide effect',
						'type' => Control_Type::SELECT,
						'options' => [
							'fade' => 'Fade',
							'slide' => 'Slide',
							'cube' => 'Cube',
							'coverflow' => 'Coverflow',
							'flip' => 'Flip',
						],
						'default' => 'fade',
					]
				]
			],
			[
				'id' => 'sc_content',
				'label' => 'Content',
				'controls' => [
					[
						'id' => 'sc_content_cards',
						'type' => Control_Type::REPEATER,
						'title_field' => 'Card',
						'controls' => [
							[
								'id' => 'sc_content_card_title',
								'label' => 'Title',
								'type' => Control_Type::TEXT,
							],
							[
								'id' => 'sc_content_card_second_title',
								'label' => 'Second title',
								'type' => Control_Type::TEXT,
							],
							[
								'id' => 'sc_content_card_image_bg',
								'label' => 'Image background',
								'type' => Control_Type::MEDIA,
							],
							[
								'id' => 'sc_content_card_image',
								'label' => 'Image',
								'type' => Control_Type::MEDIA,
							],
                            [
								'id' => 'sc_content_card_icon',
								'label' => 'Icon',
								'type' => Control_Type::ICON,
							],
							[
								'id' => 'sc_content_card_button',
								'label' => 'Button text',
								'type' => Control_Type::TEXT,
							],
							[
								'id' => 'sc_content_card_button_url',
								'label' => 'Button url',
								'type' => Control_Type::URL,
							],
							[
								'id' => 'sc_content_card_button_class',
								'label' => 'Button custom class',
								'type' => Control_Type::TEXT,
							],
							[
								'id' => 'sc_content_card_copy',
								'label' => 'Copy',
								'type' => Control_Type::EDITOR,
							],
							[
								'id' => 'sc_content_card_template',
								'label' => 'Template',
								'type' => Control_Type::TEMPLATE,
							],
						]
					],
				]
			],
		];
	}

	protected function style_controls() {
		$id = '.ecp__container__sc';
		return [
			[
				'id' => 'sc_style_global',
				'label' => 'Global',
				'controls' => [
					[
						'id' => 'sc_style_global_spacing',
						'type' => Control_Type::SPACING,
						'class' => $id . ' .ecp__container',
					],
					[
						'id' => 'sc_style_global_border',
						'type' => Control_Type::BORDER,
						'class' => $id . ' .ecp__container',
					],
				]
			],
			[
				'id' => 'sc_style_card',
				'label' => 'Slide card',
				'controls' => [
					[
						'id' => 'sc_style_card_tab',
						'type' => Control_Type::TABS,
						'tabs' => [
							[
								'id' => 'sc_style_card_outer',
								'label' => 'Outer card',
								'class' => $id . ' .ecp__container .ecp__container__card',
							],
							[
								'id' => 'sc_style_card_inner',
								'label' => 'Inner card',
								'class' => $id . ' .ecp__container .ecp__container__card .ecp__slide__card__inner',
							],
						],
						'controls' => [
							['id' => 'spacing', 'type' => Control_Type::SPACING],
							['id' => 'border', 'type' => Control_Type::BORDER],
							['id' => 'flex_box', 'type' => Control_Type::FLEXBOX],
							['id' => 'background', 'type' => Control_Type::BACKGROUND],
						]
					]
				]
			],
			[
				'id' => 'sc_style_btn',
				'label' => 'Slide button',
				'controls' => [
					[
						'id' => 'sc_style_btn_position', 
						'type' => Control_Type::POSITION,
						'class' => $id . ' .ecp__container .ecp__container__actions'
					],
					[
						'id' => 'sc_style_btn_tab',
						'type' => Control_Type::TABS,
						'tabs' => [
							[
								'id' => 'sc_style_btn_tab_normal',
								'label' => 'Normal',
								'class' => $id . ' .ecp__container .ecp__actions__btn',
							],
							[
								'id' => 'sc_style_btn_tab_hover',
								'label' => 'Hover',
								'class' => $id . ' .ecp__container .ecp__actions__btn:hover',
							],
						],
						'controls' => [
							['id' => 'spacing', 'type' => Control_Type::SPACING],
							['id' => 'border', 'type' => Control_Type::BORDER],
							['id' => 'background', 'type' => Control_Type::BACKGROUND],
						]
					]
				]
			],
			[
				'id' => 'sc_style_card_image',
				'label' => 'Card image',
				'controls' => [
					[
						'id' => 'sc_style_card_image_tab',
						'type' => Control_Type::TABS,
						'tabs' => [
							[
								'id' => 'sc_style_card_image_outer',
								'label' => 'Outer image',
								'class' => $id . ' .ecp__container__card .ecp__slide__img',
							],
							[
								'id' => 'sc_style_card_image_inner',
								'label' => 'Inner image',
								'class' => $id . ' .ecp__container__card .ecp__slide__card__inner .ecp__card__image',
							],
						],
						'controls' => [
							['id' => 'position', 'type' => Control_Type::POSITION],
							['id' => 'image_style', 'type' => Control_Type::IMAGE_STYLE],
						]
					]
				]
			],
			[
				'id' => 'sc_style_card_title',
				'label' => 'Card title',
				'controls' => [
					[
						'id' => 'sc_style_card_title_typography',
						'type' => Control_Type::TYPOGRAPHY,
						'class' => $id . ' .ecp__container .ecp__card__title h3',
					],
					[
						'id' => 'sc_style_card_title_spacing',
						'type' => Control_Type::SPACING,
						'class' => $id . ' .ecp__container .ecp__card__title h3',
					],
					[
						'id' => 'sc_style_card_title_border',
						'type' => Control_Type::BORDER,
						'class' => $id . ' .ecp__container .ecp__card__title h3',
					],
				]
			],
			[
				'id' => 'sc_style_card_second_title',
				'label' => 'Card second title',
				'controls' => [
					[
						'id' => 'sc_style_card_second_title_typography',
						'type' => Control_Type::TYPOGRAPHY,
						'class' => $id . ' .ecp__container .ecp__card__second__title h4',
					],
					[
						'id' => 'sc_style_card_second_title_spacing',
						'type' => Control_Type::SPACING,
						'class' => $id . ' .ecp__container .ecp__card__second__title h4',
					],
					[
						'id' => 'sc_style_card_second_title_border',
						'type' => Control_Type::BORDER,
						'class' => $id . ' .ecp__container .ecp__card__second__title h4',
					],
				]
			],
			[
				'id' => 'sc_style_card_copy',
				'label' => 'Card content',
				'controls' => [
					[
						'id' => 'sc_style_card_copy_typography',
						'type' => Control_Type::TYPOGRAPHY,
						'class' => $id . ' .ecp__container .ecp__card__copy',
					],
					[
						'id' => 'sc_style_card_copy_spacing',
						'type' => Control_Type::SPACING,
						'class' => $id . ' .ecp__container .ecp__card__copy',
					],
					[
						'id' => 'sc_style_card_copy_border',
						'type' => Control_Type::BORDER,
						'class' => $id . ' .ecp__container .ecp__card__copy',
					],
				]
			],
			[
				'id' => 'sc_style_card_template',
				'label' => 'Card template',
				'controls' => [
					[
						'id' => 'sc_style_card_template_spacing',
						'type' => Control_Type::SPACING,
						'class' => $id . ' .ecp__container .ecp__card__template',
					],
					[
						'id' => 'sc_style_card_template_border',
						'type' => Control_Type::BORDER,
						'class' => $id . ' .ecp__container .ecp__card__template',
					],
				]
			],
			[
				'id' => 'sc_style_card_btn',
				'label' => 'Card button',
				'controls' => [
					[
						'id' => 'sc_style_card_btn_spacing',
						'type' => Control_Type::SPACING,
						'class' => $id . ' .ecp__container .ecp__card__btn',
					],
					[
						'id' => 'sc_style_card_btn_border',
						'type' => Control_Type::BORDER,
						'class' => $id . ' .ecp__container .ecp__card__btn',
					],
					[
						'id' => 'sc_style_card_btn_background',
						'type' => Control_Type::BACKGROUND,
						'class' => $id . ' .ecp__container .ecp__card__btn',
					],
					[
						'id' => 'sc_style_card_btn_typography',
						'type' => Control_Type::TYPOGRAPHY,
						'class' => $id . ' .ecp__container .ecp__card__btn',
					],
				]
			],
			
		];
	}
}

Plugin::instance()->widgets_manager->register(new ECP_Slider_Card());
