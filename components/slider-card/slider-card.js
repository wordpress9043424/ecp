jQuery(function ($) {
  $('.ecp__container__sc').each(function () {
    const slider = $(this);
    const id = slider.attr('id');
    const config = JSON.parse(slider.attr('config') || '{}');
    const nextEl = slider.find('.ecp__actions__btn.--right')[0];
    const prevEl = slider.find('.ecp__actions__btn.--left')[0];
    const containerActions = slider.find('.ecp__container__actions');
    let containerNavigation = slider.find('[navigation]');
    containerNavigation.css('width', `${containerActions.outerWidth()}px`);
    containerNavigation.css('height', `${containerActions.outerHeight()}px`);

    const setAutomaticHeight = () => {
      setTimeout(setNavigation, 50);
      // let maxHeight = 0;
      // const slides = slider.find('.swiper-slide');
      //
      // slides.each((i, slide) => {
      //   const slideHeight = $(slide).innerHeight();
      //   maxHeight = slideHeight > maxHeight ? slideHeight : maxHeight;
      // });

      // slides.css('height', `${maxHeight}px`);
    }

    const setNavigation = () => {
      if (config?.navigationCard === 'inner_card') {
        containerNavigation.css('width', containerActions.width())
        const position = containerNavigation.position()
        containerActions.css('position', 'absolute');
        containerActions.css('top', `${position.top}px`);
        containerActions.css('left', `${position.left}px`);
      }
    }

    $(nextEl)?.on('click', () => {
      setTimeout(() => {
        containerNavigation = slider.find('.swiper-slide-active [navigation]')
        setNavigation()
      }, 100);
    })
    $(prevEl)?.on('click', () => {
      setTimeout(() => {
        containerNavigation = slider.find('.swiper-slide-active [navigation]')
        setNavigation()
      }, 100);
    })

    setNavigation();
    const conf = {
      slidesPerView: 1,
      loop: config?.loop || true,
      navigation: {
        nextEl,
        prevEl,
      },
      effect: config?.slideEffect || 'slide',
      on: {
        init: setAutomaticHeight
      }
    }
    new Swiper(`#${id} .ecp__container`, conf);
    $(window).on('resize', setAutomaticHeight);

  });
});
