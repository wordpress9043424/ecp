<?php
namespace ECP\Components;

use Elementor\Plugin;
use ECP\Enum\Control_Type;
use Elementor\Icons_Manager;
use ECP\Utils\ECP_Widget_Base;
use Elementor\Group_Control_Image_Size;

if ( ! defined( 'ABSPATH' ) ) exit;

class ECP_Toggle extends ECP_Widget_Base
{

	public function get_name()
	{
		return 'ecp_toggle';
	}

	public function get_title()
	{
		return esc_html__('Toggle', 'ecp-widget');
	}

	public function get_icon()
	{
		return 'eicon-toggle';
	}

	public function get_categories()
	{
		return ['ecp-category'];
	}

	public function get_style_depends()
	{
		return ['ecp-toggle-css'];
	}

	public function get_script_depends()
	{
		return ['ecp-toggle-js'];
	}

	protected function render() {
		$settings = $this->get_active_settings();
		$config = json_encode([]);
		?>
		<div id="ecp_<?php echo $this->get_id(); ?>" class="ecp__container__toggle2" config='<?php echo $config; ?>'>
			<ul class="ecp__container">
				<?php
				foreach($settings['ecp_toggle_cards'] as $index => $item) {
					$this->render_tab($item, $index + 1);
				}
				?>
			</ul>
		</div>
		<?php
	}

	private function render_tab($card, $index) {
		?>
		<li class="container__element" aria-expanded="false">
		<div class="collapsible">
			<input type="checkbox" id="checkbox_<?php echo $this->get_id() + $index; ?>" class="collapsible__input">
			<label for="checkbox_<?php echo $this->get_id() + $index; ?>" class="collapsible__head">
				<?php
					if ($card['ecp_toggle_cards_header_counter'] === 'yes') {
						echo "<p class='ecp__counter'>{$index}</p>";
					}
				?>
				<h2 class="ecp_text"><?php echo esc_html($card['ecp_toggle_cards_header_text']); ?></h2>
			</label>

			<div class="collapsible__content">
				<?php
					switch($card['ecp_toggle_cards_content_opt']) {
						case 'image':
							$this->render_image($card);
							break;
					}
				?>
			</div>
		</div>
		</li>
		<?php
	}

	protected function render_image($card)
	{
		$image_src = Group_Control_Image_Size::get_attachment_image_src(
			$card['ecp_toggle_cards_content_image']['id'],
			'back_image_size',
			$card
		);
		if ($image_src) {

			$image_src = $card['ecp_toggle_cards_content_image']['url'];
			echo '<img class="ecp__card__img" alt="' . esc_attr($image_src) . '" src="' . esc_url($image_src) . '"/>';
		}
	}

	protected function content_controls() {
		return [
			[
				'id' => 'toggle_content',
				'label' => 'Content',
				'controls' => [
					[
						'id' => 'toggle_cards',
						'type' => Control_Type::REPEATER,
						'title_field' => 'Card',
						'controls' => [
							[
								'id' => 'toggle_cards_header_counter',
								'label' => 'Add counter',
								'type' => Control_Type::SWITCHER,
							],
							[
								'id' => 'toggle_cards_header_text',
								'label' => 'Text',
								'type' => Control_Type::TEXT,
							],
							[
								'id' => 'toggle_cards_content_opt',
								'label' => 'Content options',
								'type' => Control_Type::SELECT,
								'options' => [
									'image' => __('Image', 'ecp-widget'),
								],
								'default' => 'image',
							],
							[
								'id' => 'toggle_cards_content_image',
								'label' => 'Image',
								'type' => Control_Type::MEDIA,
							],
						]
					],
				]
			],
		];
	}

	protected function style_controls() {
		$id = '.ecp__container__toggle2';
		return [
			[
				'id' => 'toggle_style_global',
				'label' => 'Global',
				'class' => $id . ' .ecp__container',
				'controls' => [
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
          ['type' => Control_Type::BACKGROUND],
				]
			],
			[
				'id' => 'toggle_style_element',
				'label' => 'List Element',
				'class' => $id . ' .ecp__container .container__element',
				'controls' => [
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
          ['type' => Control_Type::BACKGROUND],
				]
			],
			[
				'id' => 'toggle_style_header',
				'label' => 'Header',
				'class' => $id . ' .ecp__container .collapsible__head',
				'controls' => [
					['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
          ['type' => Control_Type::BACKGROUND],
					['type' => Control_Type::FLEXBOX],
				]
			],
			[
				'id' => 'toggle_style_card_hc',
				'label' => 'Header conunter',
				'class' => $id . ' .ecp__container .collapsible__head .ecp__counter',
				'controls' => [
					['type' => Control_Type::SPACING],
          ['type' => Control_Type::TYPOGRAPHY],
				]
			],
			[
				'id' => 'toggle_style_card_ht',
				'label' => 'Header text',
				'class' => $id . ' .ecp__container .collapsible__head .ecp_text',
				'controls' => [
					['type' => Control_Type::SPACING],
          ['type' => Control_Type::TYPOGRAPHY],
				]
			],
			[
				'id' => 'toggle_style_content',
				'label' => 'Content IMG',
				'class' => $id . ' .ecp__container .collapsible__content .ecp__card__img',
				'controls' => [
					['type' => Control_Type::SPACING],
					['type' => Control_Type::SIZE],
					['type' => Control_Type::IMAGE_STYLE],
				]
			],
		];
	}
}

Plugin::instance()->widgets_manager->register(new ECP_Toggle());
