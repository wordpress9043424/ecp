jQuery(function ($) {
  const onCheckboxChange = (event) => {
    const elem = $(event.target).closest('.container__element')
    elem.attr('aria-expanded', event?.target?.checked);
    const content = elem.find('.collapsible__content');
    content.slideToggle(300).css('display', 'flex');
  }

  $('.ecp__container__toggle2').each(function () {
    const checkboxes =  $(this).find('input[type="checkbox"]');
    checkboxes.each(function () {
      $(this).on('change', onCheckboxChange);
    });

    $(window).on('beforeunload', function () {
      checkboxes.each(function () {
        $(this).off('change', onCheckboxChange);
      });
    });
  });
});