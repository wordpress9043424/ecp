<?php

namespace ecp\components\posts;

use Elementor\Controls_Manager;
use WP_Query;
use Elementor\Plugin;
use ECP\Enum\Control_Type;
use ECP\Utils\ECP_Widget_Base;

if (!defined('ABSPATH')) exit;

class ECP_Posts extends ECP_Widget_Base
{
  
  public function get_name(): string
  {
    return 'ecp_posts_v2';
  }
  
  public function get_title(): string
  {
    return esc_html__('Posts', 'ecp-widget');
  }
  
  public function get_icon(): string
  {
    return 'eicon-media-carousel';
  }
  
  public function get_categories(): array
  {
    return ['ecp-category'];
  }
  
  public function get_style_depends(): array
  {
    return ['ecp-posts-css'];
  }
  
  public function get_script_depends(): array
  {
    return ['ecp-posts-js'];
  }
  
  protected function render(): void
  {
    $ID = $this->getId();
    $settings = $this->get_active_settings();
    $postPerPage = $this->getResponsiveValue($settings, 'ecp_posts_global_posts_per_page');
    $query = new WP_Query([
      'post_type' => $settings['ecp_posts_global_select_type'],
      'posts_per_page' => $postPerPage['767'],
      'paged' => 1,
      'order' => $settings['ecp_posts_global_select_order'],
      'orderby' => $settings['ecp_posts_global_select_order_by'],
      'post_status' => 'publish',
    ]);
    $total = ceil($query->found_posts/$postPerPage['767'] ?? 1 );
    $this->addJsConfig('ecp-posts-js', [
      'page' => '1',
      'total' => $total,
      'order' => $settings['ecp_posts_global_select_order'],
      'orderBy' => $settings['ecp_posts_global_select_order_by'],
      'postsPerPage' => $postPerPage['767'],
      'postPerSwiper' => $this->getResponsiveValue($settings, 'ecp_posts_global_posts_swiper_count'),
      'elementor' => $settings
    ]);
    
    $postMode = $this->getResponsiveValue($settings, 'ecp_posts_global_select_mode');
    echo "<div data-id='$ID' class='ecp__container__post-v2'>";
      echo "<div class='ecp__container__head'>";
        $this->renderHeader($settings['ecp_posts_header_element']);
      echo "</div>";
    
      foreach($postMode as $key => $mode) {
        echo "<div mode='$mode' device='$key' class='ecp__container'>";
        switch ($mode) {
          case 'composition_card':
            $this->renderGridCard($query, $settings);
            break;
          case 'slider':
            $this->renderSlider($query, $settings);
            break;
          case 'scroll_horizon':
            $this->renderScrollHorizon($query, $settings);
            break;
        }
        echo '</div>';
      }
      
      echo "<div class='ecp__container__footer'>";
        $this->renderFooter($settings['ecp_posts_footer_element'], $total);
      echo "</div>";
    echo '</div>';
  }
  
  private function renderGridCard($query, $settings): void{
    echo "<div class='ecp__container__body'>";
      echo "<ul id='ecp-ul-post' class='ecp__posts'>";
        while ($query->have_posts()) {
          $query->the_post();
          echo "<li>";
          renderPost(
            $settings,
            get_the_post_thumbnail_url(post: get_the_ID(), size: 'full'),
          );
          echo "</li>";
        }
      echo "</ul>";
    echo "</div>";
  }
  
  private function renderSlider($query, $settings): void{
    echo "<div class='ecp__container__body swiper'>";
      echo "<div class='ecp_posts swiper-wrapper'>";
        while ($query->have_posts()) {
          $query->the_post();
          
          echo "<div class='ecp__post swiper-slide'>";
          renderPost(
            $settings,
            get_the_post_thumbnail_url(post: get_the_ID(), size: 'full'),
          );
          echo "</div>";
        }
      echo "</div>";
    echo "</div>";
  }
  
  private function renderScrollHorizon($query, $settings): void{
    echo "<div class='ecp__container__body'>";
      echo "<div class='ecp_posts'>";
        while ($query->have_posts()) {
          $query->the_post();
          echo '<div class="ecp__post">';
          renderPost(
            $settings,
            get_the_post_thumbnail_url(post: get_the_ID(), size: 'full'),
          );
          echo '</div>';
        }
      echo "</div>";
    echo "</div>";
  }
  
  private function renderHeader($header): void
  {
    foreach($header as $element) {
      switch($element['ecp_posts_header_select_field_type']) {
        case 'label':
          $this->renderLabel($element['ecp_posts_header_label']);
          break;
        case 'select':
          $this->renderSelect($element);
          break;
        case 'search':
          $this->renderSearch($element);
          break;
        case 'spacer':
          echo "<div class='ecp__spacer'></div>";
          break;
      }
    }
  }
  
  private function renderLabel($title): void
  {
    echo "<h2 class='ecp__header__label'>$title</h2>";
  }
  
  private function renderSelect($element): void
  {
    $label = $element['ecp_posts_header_select_label'];
    $mode = $element['ecp_posts_header_select_advanced'];
    $full = $element['ecp_posts_header_select_full'] === 'yes';
    $placeholder = $element['ecp_posts_header_select_placeholder'];
    $options = get_terms(['taxonomy' => $mode, 'hide_empty' => !$full]);
    $removable = $element['ecp_posts_header_select_removable'] === 'yes';
    
    echo "<div class='ecp__container__select {$mode}' removable='$removable' select='$mode'>";
      echo "<div class='ecp__additional__box'>";
        renderIcon($element['ecp_posts_header_select_icon_removable'], 'ecp__removable__icon');
      echo "</div>";
    echo "<label class='ecp__label'>$label</label>";
    echo "<div class='ecp__selected'>";
        echo "<p class='ecp__input' placeholder='$placeholder'>$placeholder</p>";
        renderIcon($element['ecp_posts_header_select_icon']);
      echo "</div>";

      echo "<ul class='ecp__options'>";
        foreach ($options as $option) {
          $slug = $option->slug;
          $name = $option->name;
          echo "<li class='ecp__option' value='$slug'>$name</li>";
        }
      echo "</ul>";
    echo "</div>";
  }
  
  private function renderSearch($element): void
  {
    $class = $element['ecp_posts_header_search_btn_class'];
    $text = $element['ecp_posts_header_search_btn_text'];
    $placeholder = $element['ecp_posts_header_search_placeholder'];
    echo "<div class='ecp__container__search'>";
      echo "<div class='ecp__search'>";
        renderIcon($element['ecp_posts_header_search_icon']);
        echo "<input class='ecp__search__input' placeholder='$placeholder' />";
      echo "</div>";
      
      echo "<a class='ecp__search__btn $class'>$text</a>";
    echo "</div>";
  }
  
  private function renderFooter($footer, $totalPage): void
  {
    foreach($footer as $element) {
      switch($element['ecp_posts_footer_select_field_type']) {
        case 'page_counter':
          $this->renderPageCounter($totalPage);
          break;
        case 'select_page':
          $this->renderFooterSelect(
            $element['ecp_posts_footer_select_label'],
            $element['ecp_posts_footer_select_icon'],
          );
          break;
        case 'pagination':
          $this->renderPagination($totalPage);
          break;
        case 'navigation':
          $this->renderNavigation(
            $element['ecp_posts_footer_navigation_left_icon'],
            $element['ecp_posts_footer_navigation_right_icon'],
            $element['ecp_posts_footer_navigation_mode'],
            $totalPage
          );
          break;
        case 'spacer':
          echo "<div class='ecp__spacer'></div>";
          break;
      }
    }
  }
  
  private function renderPageCounter($totalPage): void
  {
    echo "<p class='ecp__footer__page-counter'>1/$totalPage</p>";
  }
  
  private function renderFooterSelect($text, $icon): void
  {
    echo "<div class='ecp__footer__select>";
    echo "<p class='ecp__select_text'>$text</p>";
    echo "<div class='ecp__select'>";
    echo "<div class='ecp__selected'>4";
    renderIcon($icon);
    echo "</div>";
    
    echo "<ul class='ecp__options'>";
    $class = '--active';
    foreach ([4, 8, 12, 16, 20] as $value) {
      echo "<li class='ecp__option $class'>$value</li>";
      $class = '';
    }
    echo "</ul>";
    echo "</div>";
    echo "</div>";
  }
  
  private function renderPagination($totalPage): void
  {
    echo "<div class='ecp__pagination'>";
    $class = '--active';
    for ($i = 1; $i <= $totalPage; $i++) {
      echo "<p value='$i' class='ecp__bullet $class'></p>";
      $class = '';
    }
    echo "</div>";
  }
  
  private function renderNavigation($prevIcon, $nextIcon, $mode, $totalPage): void
  {
    echo "<div class='ecp__navigation'>";
    renderIcon($prevIcon, 'ecp__navigation__btn --prev', 'disabled="disabled"');
    if (!empty($mode) && $mode !== 'btn') {
      echo "<div class='ecp__navigation__pages'></div>";
    }
    renderIcon($nextIcon, 'ecp__navigation__btn --next', $totalPage == 1 ? 'disabled="disabled"' : '');
    echo "</div>";
  }
  
  protected function content_controls(): array
  {
    return [
      [
        'id' => 'posts_global',
        'label' => 'Global',
        'controls' => [
          [
            'id' => 'posts_global_select_mode',
            'label' => 'Select display mode',
            'type' => Control_Type::SELECT,
            'options' => [
              'composition_card' => __('Grid/Flex posts', 'ecp-widget'),
              'slider' => __('Slider', 'ecp-widget'),
              'scroll_horizon' => __('Scroll horizon', 'ecp-widget'),
            ],
            'responsive' => true,
            'default' => 'composition_card',
          ],
          [
            'id' => 'posts_global_select_type',
            'label' => 'Select post type',
            'type' => Control_Type::SELECT,
            'options' => $this->getPostTypes(),
            'default' => 'post',
          ],
          [
            'id' => 'posts_global_select_order',
            'label' => 'Order',
            'type' => Control_Type::SELECT,
            'options' => [
              'DESC' => __('Descending', 'ecp-widget'),
              'ASC' => __('Ascending', 'ecp-widget'),
            ],
            'default' => 'DESC',
          ],
          [
            'id' => 'posts_global_select_order_by',
            'label' => 'Order by',
            'type' => Control_Type::SELECT,
            'options' => [
              'date' => __('Date', 'ecp-widget'),
              'title' => __('Title', 'ecp-widget'),
              'author' => __('Author', 'ecp-widget'),
              'comment_count' => __('Comment count', 'ecp-widget'),
              'modified' => __('Modified', 'ecp-widget'),
              'rand' => __('Random', 'ecp-widget'),
            ],
            'default' => 'date',
          ],
          [
            'id' => 'posts_global_posts_per_page',
            'label' => 'Posts count per page',
            'type' => Control_Type::NUMBER,
            'responsive' => true,
          ],
          [
            'id' => 'posts_global_posts_swiper_count',
            'label' => 'Amount of posts in swiper',
            'type' => Control_Type::NUMBER,
            'responsive' => true,
            'condition' => [
              'ecp_posts_global_select_mode' => 'slider'
            ]
          ],
        ]
      ],
      [
        'id' => 'posts_content',
        'label' => 'Content',
        'controls' => [
          [
            'id' => 'posts_content_field',
            'type' => Control_Type::REPEATER,
            'title_field' => 'Field type - {{{ecp_posts_content_select_field_type}}}',
            'controls' => [
              [
                'id' => 'posts_content_select_field_type',
                'label' => 'Field type',
                'type' => Control_Type::SELECT,
                'options' => [
                  'image' => __('Image', 'ecp-widget'),
                  'title' => __('Title', 'ecp-widget'),
                  'excerpt' => __('Excerpt', 'ecp-widget'),
                  'button' => __('Button', 'ecp-widget'),
                  'date' => __('Date', 'ecp-widget'),
                ],
                'default' => 'title',
              ],
              [
                'id' => 'posts_content_button_text',
                'label' => 'Button text',
                'type' => Control_Type::TEXT,
                'condition' => [
                  'ecp_posts_content_select_field_type' => 'button'
                ]
              ],
              [
                'id' => 'posts_content_button_class',
                'label' => 'Button class',
                'type' => Control_Type::TEXT,
                'condition' => [
                  'ecp_posts_content_select_field_type' => 'button'
                ]
              ],
              [
                'id' => 'posts_content_date_type',
                'label' => 'Date type',
                'type' => Control_Type::SELECT,
                'options' => [
                  'd/m/Y' => 'dd/mm/yyyy',
                ],
                'default' => 'd/m/Y',
                'condition' => [
                  'ecp_posts_content_select_field_type' => 'date'
                ]
              ],
            ]
          ],
        ]
      ],
      [
        'id' => 'posts_header',
        'label' => 'Header',
        'controls' => [
          [
            'id' => 'posts_header_element',
            'type' => Control_Type::REPEATER,
            'title_field' => 'Field type - {{{ecp_posts_header_select_field_type}}}',
            'controls' => [
              [
                'id' => 'posts_header_select_field_type',
                'label' => 'Field type',
                'type' => Control_Type::SELECT,
                'options' => [
                  'label' => __('Label', 'ecp-widget'),
                  'select' => __('Select', 'ecp-widget'),
                  'search' => __('Search', 'ecp-widget'),
                  'spacer' => __('Spacer', 'ecp-widget'),
                ],
                'default' => 'label',
              ],
              [
                'id' => 'posts_header_select_advanced',
                'label' => 'Select mode',
                'type' => Control_Type::SELECT,
                'options' => [
                  'category' => __('Category', 'ecp-widget'),
                  'post_tag' => __('Tag', 'ecp-widget'),
                ],
                'default' => 'category',
                'condition' => ['ecp_posts_header_select_field_type' => 'select']
              ],
              [
                'id' => 'posts_header_select_label',
                'label' => 'Select label',
                'type' => Control_Type::TEXT,
                'condition' => ['ecp_posts_header_select_field_type' => 'select']
              ],
              [
                'id' => 'posts_header_select_placeholder',
                'label' => 'Select placeholder',
                'type' => Control_Type::TEXT,
                'default' => 'Select',
                'condition' => ['ecp_posts_header_select_field_type' => 'select']
              ],
              [
                'id' => 'posts_header_select_icon',
                'label' => 'Select icon',
                'type' => Control_Type::ICON,
                'default' => [
                  'value' => 'fas fa-chevron-down',
                  'library' => 'fa-solid',
                ],
                'condition' => ['ecp_posts_header_select_field_type' => 'select']
              ],
              [
                'id' => 'posts_header_select_icon_removable',
                'label' => 'Select icon removable',
                'type' => Control_Type::ICON,
                'condition' => ['ecp_posts_header_select_field_type' => 'select']
              ],
              [
                'id' => 'posts_header_select_removable',
                'label' => 'Removable',
                'type' => Control_Type::SWITCHER,
                'condition' => ['ecp_posts_header_select_field_type' => 'select']
              ],
              [
                'id' => 'posts_header_select_full',
                'label' => 'Show those not assigned to posts',
                'type' => Control_Type::SWITCHER,
                'condition' => ['ecp_posts_header_select_field_type' => 'select']
              ],
              [
                'id' => 'posts_header_label',
                'label' => 'Label',
                'type' => Control_Type::TEXT,
                'condition' => ['ecp_posts_header_select_field_type' => 'label']
              ],
              [
                'id' => 'posts_header_search_icon',
                'label' => 'Icon for search',
                'type' => Control_Type::ICON,
                'default' => [
                  'value' => 'fas fa-search',
                  'library' => 'fa-solid',
                ],
                'condition' => ['ecp_posts_header_select_field_type' => 'search']
              ],
              [
                'id' => 'posts_header_search_placeholder',
                'label' => 'Placeholder',
                'type' => Control_Type::TEXT,
                'condition' => ['ecp_posts_header_select_field_type' => 'search']
              ],
              [
                'id' => 'posts_header_search_btn_text',
                'label' => 'Button text',
                'type' => Control_Type::TEXT,
                'condition' => ['ecp_posts_header_select_field_type' => 'search']
              ],
              [
                'id' => 'posts_header_search_btn_class',
                'label' => 'Button additional class',
                'type' => Control_Type::TEXT,
                'condition' => ['ecp_posts_header_select_field_type' => 'search']
              ],
            ]
          ],
        ]
      ],
      [
        'id' => 'posts_footer',
        'label' => 'Footer',
        'controls' => [
          [
            'id' => 'posts_footer_element',
            'type' => Control_Type::REPEATER,
            'title_field' => 'Field type - {{{ecp_posts_footer_select_field_type}}}',
            'controls' => [
              [
                'id' => 'posts_footer_select_field_type',
                'label' => 'Field type',
                'type' => Control_Type::SELECT,
                'options' => [
                  'page_counter' => __('Page counter', 'ecp-widget'),
                  'select_page' => __('Select page', 'ecp-widget'),
                  'pagination' => __('Pagination', 'ecp-widget'),
                  'navigation' => __('Navigation', 'ecp-widget'),
                  'spacer' => __('Spacer', 'ecp-widget'),
                ],
                'default' => 'page_counter',
              ],
              [
                'id' => 'posts_footer_navigation_mode',
                'label' => 'Select mode',
                'type' => Control_Type::SELECT,
                'options' => [
                  'btn' => __('Only buttons', 'ecp-widget'),
                  'btn_page' => __('Buttons + pages', 'ecp-widget'),
                  'only_page' => __('Pages', 'ecp-widget'),
                ],
                'default' => 'btn',
                'condition' => ['ecp_posts_footer_select_field_type' => 'navigation']
              ],
              [
                'id' => 'posts_footer_select_label',
                'label' => 'Select label',
                'type' => Control_Type::TEXT,
                'condition' => [
                  'ecp_posts_footer_select_field_type' => 'select_page',
                ]
              ],
              [
                'id' => 'posts_footer_select_icon',
                'label' => 'Select icon',
                'type' => Control_Type::ICON,
                'condition' => [
                  'ecp_posts_footer_select_field_type' => 'select_page',
                ]
              ],
              [
                'id' => 'posts_footer_navigation_left_icon',
                'label' => 'Icon prev',
                'type' => Control_Type::ICON,
                'condition' => [
                  'ecp_posts_footer_select_field_type' => 'navigation',
                  'ecp_posts_footer_navigation_mode!' => 'only_page',
                ]
              ],
              [
                'id' => 'posts_footer_navigation_right_icon',
                'label' => 'Icon next',
                'type' => Control_Type::ICON,
                'condition' => [
                  'ecp_posts_footer_select_field_type' => 'navigation',
                  'ecp_posts_footer_navigation_mode!' => 'only_page',
                ]
              ],
            ]
          ],
        ]
      ]
    ];
  }
  
  protected function style_controls(): array
  {
    $class = '.ecp__container__post-v2';
    return [
      [
        'id' => 'posts_style_global',
        'label' => 'Global',
        'class' => $class . ' .ecp__container__body',
        'controls' => [
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
          ['type' => Control_Type::BACKGROUND],
        ]
      ],
      [
        'id' => 'posts_style_posts',
        'label' => 'Posts list',
        'class' => $class . ' .ecp__container__body .ecp__posts',
        'controls' => [
          ['type' => Control_Type::COMPOSITION],
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
          ['type' => Control_Type::BACKGROUND],
        ]
      ],
      [
        'id' => 'posts_style_post',
        'label' => 'Post',
        'class' => $class . ' .ecp__container__body .ecp__posts .ecp__post',
        'controls' => [
          ['type' => Control_Type::FLEXBOX],
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
          ['type' => Control_Type::BACKGROUND],
        ]
      ],
      [
        'id' => 'posts_style_post_image',
        'label' => 'Post image',
        'class' => $class . ' .ecp__container__body .ecp__posts .ecp__post .ecp__post__img',
        'controls' => [
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
          ['type' => Control_Type::IMAGE_STYLE],
          ['type' => Control_Type::SIZE],
        ]
      ],
      [
        'id' => 'posts_style_post_title',
        'label' => 'Post title',
        'class' => $class . ' .ecp__container__body .ecp__posts .ecp__post .ecp__post__title',
        'controls' => [
          ['type' => Control_Type::TYPOGRAPHY],
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
        ]
      ],
      [
        'id' => 'posts_style_post_excerpt',
        'label' => 'Post excerpt',
        'class' => $class . ' .ecp__container__body .ecp__posts .ecp__post .ecp__post__excerpt',
        'controls' => [
          ['type' => Control_Type::TYPOGRAPHY],
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
        ]
      ],
      [
        'id' => 'posts_style_post_date',
        'label' => 'Post date',
        'class' => $class . ' .ecp__container__body .ecp__posts .ecp__post .ecp__post__date',
        'controls' => [
          ['type' => Control_Type::TYPOGRAPHY],
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
        ]
      ],
      [
        'id' => 'posts_style_post_btn',
        'label' => 'Post button',
        'controls' => [
          [
            'id' => 'posts_style_post_btn_tab',
            'label' => 'Post button',
            'type' => Control_Type::TABS,
            'tabs' => [
              [
                'id' => 'posts_style_post_btn_normal',
                'label' => 'Normal',
                'class' => $class . ' .ecp__container__body .ecp__posts .ecp__post .ecp__post__btn',
              ],
              [
                'id' => 'posts_style_post_btn_hover',
                'label' => 'Hover',
                'class' => $class . ' .ecp__container__body .ecp__posts .ecp__post .ecp__post__btn:hover',
              ],
            ],
            'controls' => [
              ['type' => Control_Type::TYPOGRAPHY],
              ['type' => Control_Type::SPACING],
              ['type' => Control_Type::BORDER],
              ['type' => Control_Type::BACKGROUND],
            ]
          ]
        ]
      ],
      [
        'id' => 'posts_style_footer',
        'label' => 'Footer',
        'class' => $class . ' .ecp__container__footer',
        'controls' => [
          ['type' => Control_Type::FLEXBOX],
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
          ['type' => Control_Type::BACKGROUND],
        ]
      ],
      [
        'id' => 'posts_style_footer_page_counter',
        'label' => 'Footer page counter',
        'class' => $class . ' .ecp__container__footer .ecp__footer__page-counter',
        'controls' => [
          ['type' => Control_Type::TYPOGRAPHY],
          ['type' => Control_Type::SPACING],
          ['type' => Control_Type::BORDER],
        ]
      ],
      [
        'id' => 'posts_style_footer_pagination',
        'label' => 'Footer pagination',
        'controls' => [
          [
            'id' => 'posts_style_footer_pagination_space',
            'label' => 'Space',
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'range' => [
              'px' => [
                'min' => -500,
                'max' => 500,
              ],
              '%' => [
                'min' => -200,
                'max' => 200,
              ],
            ],
            'default' => [
              'unit' => 'px',
              'size' => 0,
            ],
            'selectors' => [
              '{{WRAPPER}} ' . $class . ' .ecp__container__footer .ecp__pagination' =>
                '--space-between: {{SIZE}}{{UNIT}};',
            ],
          ],
          [
            'id' => 'posts_style_footer_pagination_tab',
            'type' => Control_Type::TABS,
            'tabs' => [
              [
                'id' => 'posts_style_footer_pagination_inactive',
                'label' => 'Inactive',
                'class' => $class . ' .ecp__container__footer .ecp__pagination .ecp__bullet',
              ],
              [
                'id' => 'posts_style_footer_pagination_active',
                'label' => 'Active',
                'class' => $class . ' .ecp__container__footer .ecp__pagination .ecp__bullet.--active',
              ],
              [
                'id' => 'posts_style_footer_pagination_active_hover',
                'label' => 'Hover',
                'class' => $class . ' .ecp__container__footer .ecp__pagination .ecp__bullet:hover:not(.--active)',
              ],
            ],
            'controls' => [
              ['type' => Control_Type::BACKGROUND_COLOR],
              ['type' => Control_Type::BORDER],
              ['type' => Control_Type::SPACING],
              ['type' => Control_Type::SIZE],
              ['type' => Control_Type::TRANSFORM],
            ]
          ]
        ]
      ],
      [
        'id' => 'posts_style_footer_navigation_button',
        'label' => 'Navigation buttons',
        'controls' => [
          [
            'id' => 'posts_style_footer_navigation_button_space',
            'label' => 'Space',
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'range' => [
              'px' => [
                'min' => -500,
                'max' => 500,
              ],
              '%' => [
                'min' => -200,
                'max' => 200,
              ],
            ],
            'default' => [
              'unit' => 'px',
              'size' => 0,
            ],
            'selectors' => [
              '{{WRAPPER}} ' . $class . ' .ecp__container__footer .ecp__navigation' => '--space-between: {{SIZE}}{{UNIT}};',
            ],
          ],
          [
            'id' => 'posts_style_footer_navigation_button_tab',
            'type' => Control_Type::TABS,
            'tabs' => [
              [
                'id' => 'posts_style_footer_navigation_button_normal',
                'label' => 'Normal',
                'class' => $class . ' .ecp__container__footer .ecp__navigation .ecp__navigation__btn',
              ],
              [
                'id' => 'posts_style_footer_navigation_button_hover',
                'label' => 'Hover',
                'class' => $class . ' .ecp__container__footer .ecp__navigation .ecp__navigation__btn:hover:not([disabled="disabled"])',
              ],
              [
                'id' => 'posts_style_footer_navigation_button_disable',
                'label' => 'Disable',
                'class' => $class . ' .ecp__container__footer .ecp__navigation .ecp__navigation__btn[disabled="disabled"]',
              ]
            ],
            'controls' => [
              ['type' => Control_Type::BACKGROUND_COLOR],
              ['type' => Control_Type::BORDER],
              ['type' => Control_Type::SPACING],
              ['type' => Control_Type::ICON_STYLE],
            ]
          ]
        ]
      ],
    ];
  }
}

Plugin::instance()->widgets_manager->register(new ECP_Posts());
