jQuery(function ($) {
  $('.ecp__container__post-v2').each(function () {
    const box = $(this)
    const id = box.attr('data-id')
    const posts = box.find('.ecp__posts')
    const pagination = box.find('.ecp__pagination')
    const paginationBullet = box.find('.ecp__bullet')
    const selectTag = box.find('[select="post_tag"]')
    const searchButton = box.find('.ecp__search__btn')
    const searchInput = box.find('.ecp__search__input')
    const selectCategory = box.find('[select="category"]')
    const pageCounter = box.find('.ecp__footer__page-counter')
    const buttonPrev = box.find('.ecp__navigation__btn.--prev')
    const buttonNext = box.find('.ecp__navigation__btn.--next')

    let chosen = [];
    let activePage = 1
    let searchString = ''
    const settings = window[id]
    let total = Number(settings.total)
    const postsPerPage = Number(settings.postsPerPage)

    if (!settings || !settings.total || !settings.elementor) {
      console.error('Errors with settings')
      return
    }

    const onBullet = e => {
      const bullet = $(e.target)
      if (!bullet.hasClass('.--active')) renderNewPosts(settings.elementor, bullet.attr('value'), total)
    }

    const onSelect = (select) => e => {
      const options = select.find('.ecp__options')
      const li = options.find('li')
      if (li.length === 0 ) {
        return
      }

      const icon = $(select.find('.ecp__removable__icon')[0])
      const input = select.find('.ecp__input')
      const removable = !!select.attr('removable')
      const mode = select.attr('select')

      const remove = (slug) => {
        chosen = chosen.filter(x => x.slug !== slug)
        input.find(`[value="${slug}"]`).remove()
        options.find(`[value="${slug}"]`).removeClass('--active');
        if (chosen.length === 0) input.text(input.attr('placeholder'))
        if (options.is(':hidden')) renderNewPosts(settings.elementor, 1, total)
      }

      const add = (slug, name) => {
        chosen.push({slug, name, mode})
        chosen.length === 1 && input.text('')
        options.find(`[value="${slug}"]`).addClass('--active');
        const div = $('<div>').attr('value', slug).addClass('ecp__chosen').text(name)

        if (removable) {
          const newIcon = icon ? icon.clone() : $('<i>').addClass('ecp__removable__icon eicon-close')
          newIcon.on('click', () => remove(slug))
          div.append(newIcon)
        }
        input.append(div)
      }

      const onClose = () => {
        select.find('.ecp__selected')?.removeClass('--open')
        $(document).off('click.outsideCategory')
        li.off('click')
        renderNewPosts(settings.elementor, 1, total)
      }

      if ($(e.target).closest('.ecp__options, .ecp__removable__icon').length === 0) {
        options.slideToggle(200, () => {
          if (options.is(':visible')) {
            $(document).on('click.outsideCategory', (event) => {
              if ($(event.target).closest('.ecp__selected, .ecp__options, .ecp__removable__icon').length === 0) {
                options.slideUp(200)
                onClose()
              }
            })

            li.on('click', (eventLi) => {
              const target = $(eventLi.target)
              const slug = target.attr('value')
              if (chosen.find(x => x.slug === slug))
                remove(slug)
              else add(slug, target.text())
            })

            select.find('.ecp__selected')?.addClass('--open')
            return
          }
          onClose()
        })
      }
    }

    const onInput = e => {
      searchString = e.target.value;
    }

    const changePage = (page) => {
      renderNewPosts(settings.elementor, page, total)
    }

    const prevPage = () => activePage > 1 && changePage(--activePage)
    const nextPage = () => activePage < total && changePage(++activePage)

    const searchPosts = () => {
      renderNewPosts(settings.elementor, 1, settings.total)
    }

    const updatePage = (page)  => {
      pageCounter.text(`${page}/${total}`)
      paginationBullet.removeClass('--active')
      pagination.empty().html(paginationBullet.slice(0, total))
      box.find('.ecp__bullet').on('click', onBullet)
      box.find(`.ecp__bullet[value="${page}"]`).addClass('--active')
      buttonPrev.attr('disabled', false)
      buttonNext.attr('disabled', false)
      if (page === 1) buttonPrev.attr('disabled', true);
      if (page === total) buttonNext.attr('disabled', true)
    }

    const renderNewPosts = (settings, page) => {
      activePage = page;
      const data = {
        page,
        settings,
        postsPerPage,
        search: searchString,
        categoryName: chosen.filter(x => x.mode === 'category').map(x => x.slug).join(', '),
        postTag: chosen.filter(x => x.mode === 'post_tag').map(x => x.slug),
        action: 'ecp_get_posts'
      };

      $.post(ajax.url, data, (response) => {
        posts.fadeOut(400,
          () => {
            posts.empty().html(response === "" ? "<li><p class='ecp__no-posts'>No posts found</p></li>" : response)
            total = Number(posts.find('.ecp__additional__box').attr('total'))
            updatePage(page, total)
            posts.fadeIn(400)
          }
        )
        $('html, body').animate({ scrollTop: $('[data-id="' + id + '"]').offset().top - 100 }, 'slow');
      })
    }

    if (selectCategory.find('li').length === 0)
      selectCategory.find('.ecp__selected')?.attr('disabled', true)
    if (selectTag.find('li').length === 0)
      selectTag.find('.ecp__selected')?.attr('disabled', true)

    paginationBullet.on('click', onBullet)
    buttonPrev?.on('click', prevPage)
    buttonNext?.on('click', nextPage)
    searchInput?.on('input', onInput)
    searchButton?.on('click', searchPosts)

    selectCategory?.on('click', onSelect(selectCategory))
    selectTag?.on('click', onSelect(selectTag))
  });
});
