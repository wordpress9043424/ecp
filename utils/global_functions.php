<?php

use Elementor\Icons_Manager;

if (!function_exists(function: 'render_post')) {
  function render_post($settings, $thumbnail_url): void
  {
    foreach ($settings['p_tabs'] as $tab) {
      switch ($tab['p_tab_select']) {
        case 'date':
          echo '<p class="post__date">' . get_the_date($tab['p_tab_select_date_type']) . '</p>';
          break;
        case 'img':
          if ($thumbnail_url)
            echo '<img class="post__image" draggable="false" src="' . esc_url(url: $thumbnail_url) . '" alt="' . esc_attr(text: get_the_title()) . '">';
          else
            echo '<img class="post__image" draggable="false" src="' . esc_url(url: 'https://placehold.co/600x400') . '" alt="Placeholder">';
          break;
        case 'title':
          echo '<h1 class="post__title">' . get_the_title() . '</h1>';
          break;
        case 'excerpt':
          echo '<p class="post__excerpt">' . get_the_excerpt() . '</p>';
          break;
        case 'button':
          echo '<div><a class="post__rm ' . $settings['p_btn_custom_class'] . '" draggable="false" href="' . get_permalink() . '">' . $tab['p_tab_select_text'] . '</a></div>';
          break;
      }
    }
  }
}

if (!function_exists(function: 'renderPost')) {
  function renderPost($settings, $thumbnail_url): void
  {
    $url = get_permalink();
    echo "<a class='ecp__post' href='$url'>";
    foreach ($settings['ecp_posts_content_field'] as $tab) {
      switch ($tab['ecp_posts_content_select_field_type']) {
        case 'date':
          $date = get_the_date($tab['p_tab_select_date_type']);
          echo "<p class='ecp__post__date'>$date</p>";
          break;
        case 'image':
          $img = esc_url(!empty($thumbnail_url) ? $thumbnail_url : 'https://placehold.co/600x400');
          $title = esc_attr(get_the_title());
          echo "<img class='ecp__post__img' draggable='false' src='$img' alt='$title' />";
          break;
        case 'title':
          $title = get_the_title();
          echo "<h2 class='ecp__post__title'>$title</h2>";
          break;
        case 'excerpt':
          $excerpt = get_the_excerpt();
          echo "<p class='ecp__post__excerpt'>$excerpt</p>";
          break;
        case 'button':
          $class = $tab['ecp_posts_content_button_class'];
          $text = $tab['ecp_posts_content_button_text'];
          echo "<p class='ecp__post__btn $class' draggable='false'>$text</p>";
          break;
      }
    }
    echo "</a>";
  }
}

if (!function_exists(function: 'get_url_link_string')) {
  function get_url_link_string($url_control): string
  {
    return implode(separator: ' ', array: array_filter(array: [
      !empty($url_control['url']) ? 'href="' . esc_url($url_control['url']) . '"' : '',
      !empty($url_control['is_external']) ? 'target="_blank"' : '',
      !empty($url_control['nofollow']) ? 'rel="nofollow"' : ''
    ]));
  }
}

if (!function_exists(function: 'convert_key_value_to_string')) {
  function convert_key_value_to_string($tab): string
  {
    $result = '';
    foreach ( $tab as $attr => $value ) {
      if ( !empty( $value ) ) {
        $result .= " " . $attr . "='" . $value . "'";
      }
    }
    return  $result;
  }
}

if (!function_exists('renderIcon'))
{
  function renderIcon($icon, $class = 'ecp__icon', $attrs = ''): void
  {
    if (!empty($icon['value'])):
      echo "<p class='$class' $attrs >";
      Icons_Manager::render_icon($icon, ['aria-hidden' => 'true', ]);
      echo "</p>";
    endif;
  }
}

if (!function_exists('console')) {
  function console($data): void
  {
    echo "<script>console.log($data)</script>";
  }
}

