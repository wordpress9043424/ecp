<?php

namespace ECP\Utils;


use JetBrains\PhpStorm\NoReturn;

class ECP_Ajax_Handler {
  public function __construct() {
    add_action('wp_ajax_ecp_get_posts', [$this, 'getPosts']);
    add_action('wp_ajax_nopriv_ecp_get_posts', [$this, 'getPosts']);
  }

  public function getPosts() {
    $settings = filter_input(INPUT_POST, 'settings', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    if (empty($settings)) { wp_send_json_error(['message' => 'No settings provided']); }
    $page = filter_input(INPUT_POST, 'page', FILTER_VALIDATE_INT, ['options' => ['default' => 1]]);
    $postPerPage = filter_input(INPUT_POST, 'postsPerPage', FILTER_VALIDATE_INT, ['options' => ['default' => 1]]);
    $search = filter_input(INPUT_POST, 'search', FILTER_SANITIZE_FULL_SPECIAL_CHARS, ['options' => ['default' => '']]);
    $categoryName = filter_input(INPUT_POST, 'categoryName', FILTER_SANITIZE_FULL_SPECIAL_CHARS, ['options' => ['default' => '']]);
    $postTag = filter_input(INPUT_POST, 'postTag', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    $args = [
      's' => $search,
      'paged' => $page,
      'tag' => $postTag,
      'posts_per_page' => $postPerPage,
      'category_name' => $categoryName,
      'order' => $settings['ecp_posts_global_select_order'],
      'post_type' => $settings['ecp_posts_global_select_type'],
      'orderBy' => $settings['ecp_posts_global_select_order_by'],
      'post_status' => 'publish',
    ];
    
    $query = new \WP_Query($args);
    
    $total = ceil($query->found_posts/$postPerPage ?? 1 );
    if ($query->have_posts()) {
      while ($query->have_posts()) {
        $query->the_post();
        echo "<div class='ecp__additional__box' total='$total'></div>";
        echo "<li>";
        renderPost(
          $settings,
          get_the_post_thumbnail_url(get_the_ID(), 'full'),
        );
        echo "</li>";
      }

      wp_reset_postdata();
    }

    wp_die();
  }
}

new ECP_Ajax_Handler();
