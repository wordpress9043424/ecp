<?php
namespace ECP\Utils;

use Elementor\Plugin;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Css_Filter;

abstract class ECP_BaseClass extends Widget_Base
{
  public function __construct($data = [], $args = null)
  {
    parent::__construct($data, $args);
    if (method_exists($this, 'before_enqueue_scripts')) {
      add_action('elementor/editor/before_enqueue_scripts', [$this, 'before_enqueue_scripts']);
    }
  }


  protected function get_all_templates()
  {
    $templates = Plugin::instance()->templates_manager->get_source('local')->get_items();
		if (empty($templates)) {
			return ['0' => 'You haven’t saved templates yet.'];
		}
    $template_options = ['0' => 'Select Template'];
    foreach ($templates as $template) {
      $template_options[$template['template_id']] = $template['title'];
    }

    return $template_options;
  }

  protected function get_lang_picker()
  {
    $array = ['none' => 'None'];
    if (function_exists('pll_current_language')) {
      $array['polylang'] = 'PolyLang';
    }

    return $array;
  }

  protected function add_divider()
  {
    $this->add_control(
      uniqid('ecp-divider-'),
      [
        'type' => Controls_Manager::DIVIDER,
        'style' => 'thick',
      ]
    );
  }

  protected function add_padding($id, $class, $title = 'Padding')
  {
    $this->add_responsive_control(
      $id,
      [
        'label' => __($title, 'ecp-widget'),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%', 'em', 'custom'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );
  }

  protected function add_margin($id, $class, $title = 'Margin')
  {
    $this->add_responsive_control(
      $id,
      [
        'label' => __($title, 'ecp-widget'),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%', 'em', 'custom'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],

      ]
    );
  }

  protected function add_text($id, $class, $title = 'Text')
  {
    $this->add_responsive_control(
      $id . '_color',
      [
        'label' => __($title . ' color', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'color: {{VALUE}};',
        ],

      ]
    );

    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => $id . '_typography',
        'label' => __($title . ' typography', 'ecp-widget'),
        'selector' => '{{WRAPPER}} ' . $class,
      ]
    );

    $this->add_responsive_control(
      $id . '_whitespace',
      [
        'type' => Controls_Manager::SELECT,
        'label' => __($title . ' whitespace', 'ecp-widget'),
        'options' => [
          'normal' => __('Normal', 'ecp-widget'),
          'nowrap' => __('No wrap', 'ecp-widget'),
          'pre' => __('Pre', 'ecp-widget'),
          'pre-line' => __('Pre line', 'ecp-widget'),
          'pre-wrap' => __('Pre wrap', 'ecp-widget'),
        ],
        'default' => 'normal',
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'white-space: {{VALUE}};',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_text_decoration',
      [
        'type' => Controls_Manager::SELECT,
        'label' => __($title . ' decoration', 'ecp-widget'),
        'options' => [
          'none' => __('None', 'ecp-widget'),
          'underline' => __('Underline', 'ecp-widget'),
          'overline' => __('Overline', 'ecp-widget'),
          'line-through' => __('Line through', 'ecp-widget'),
        ],
        'default' => 'none',
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'text-decoration: {{VALUE}};',
        ],
      ]
      );
  }

  protected function add_border($id, $class, $title = 'Border')
  {
    $this->add_responsive_control(
      $id . '_radius',
      [
        'label' => __($title . ' radius', 'ecp-widget'),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],

      ]
    );

    $this->add_group_control(
      Group_Control_Border::get_type(),
      [
        'name' => $id,
        'label' => __($title, 'ecp-widget'),
        'selector' => '{{WRAPPER}} ' . $class,
      ]
    );
  }

  protected function add_outline($id, $class, $title = 'Outline') {
    $this->add_responsive_control(
      $id . '_outline_width',
      [
        'label' => __($title . ' width', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'outline-width: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_outline_style',
      [
        'label' => __($title . ' style', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'none' => __('None', 'ecp-widget'),
          'solid' => __('Solid', 'ecp-widget'),
          'dashed' => __('Dashed', 'ecp-widget'),
          'dotted' => __('Dotted', 'ecp-widget'),
          'double' => __('Double', 'ecp-widget'),
          'groove' => __('Groove', 'ecp-widget'),
          'ridge' => __('Ridge', 'ecp-widget'),
          'inset' => __('Inset', 'ecp-widget'),
          'outset' => __('Outset', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'outline-style: {{VALUE}};',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_outline_color',
      [
        'label' => __($title . ' color', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'outline-color: {{VALUE}};',
        ],
      ]
    );
  }

  protected function add_shadow($id, $class, $title = 'Shadow')
  {
    $this->add_responsive_control(
      $id . '_radius',
      [
        'label' => __($title . ' radius', 'ecp-widget'),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],

      ]
    );

    $this->add_group_control(
      Group_Control_Box_Shadow::get_type(),
      [
        'name' => $id,
        'label' => __($title, 'ecp-widget'),
        'selector' => '{{WRAPPER}} ' . $class,
      ]
    );
  }

  protected function add_inset($id, $class)
  {
    $this->add_responsive_control(
      $id . '_top',
      [
        'label' => __('Top', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'top: {{SIZE}}{{UNIT}};',
        ],

      ]

    );
    $this->add_responsive_control(
      $id . '_Bottom',
      [
        'label' => __('Bottom', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'bottom: {{SIZE}}{{UNIT}};',
        ],

      ]

    );
    $this->add_responsive_control(
      $id . '_left',
      [
        'label' => __('Left', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'left: {{SIZE}}{{UNIT}};',
        ],

      ]

    );
    $this->add_responsive_control(
      $id . '_right',
      [
        'label' => __('Right', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'right: {{SIZE}}{{UNIT}};',
        ],

      ]
    );
  }

  protected function add_custom_class($id)
  {
    $this->add_responsive_control(
      $id,
      [
        'label' => esc_html__('Custom css for btn', 'ecp-widget'),
        'type' => Controls_Manager::TEXT,

      ]
    );
  }

  protected function add_color($id, $class, $title = 'Color')
  {
    $this->add_responsive_control(
      $id,
      [
        'label' => __($title, 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'color: {{VALUE}};',
        ],

      ]
    );
  }

  protected function add_bg($id, $class, $title = 'Background color')
  {
    $this->add_responsive_control(
      $id,
      [
        'label' => __($title, 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'background-color: {{VALUE}};',
        ],

      ]
    );
  }

  protected function add_gradient($id, $class, $title = 'Gradient')
  {
    $this->add_control(
      $id . 'gradient_color_1',
      [
        'label' => __($title . '1', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}}' . $class =>
            'background-image: linear-gradient({{' . $id . 'gradient_direction.VALUE}}, {{' . $id . 'gradient_color_1.VALUE}}, {{' . $id . 'gradient_color_2.VALUE}});',
        ],
      ]
    );

    $this->add_control(
      $id . 'gradient_color_2',
      [
        'label' => __($title . ' 2', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}}' . $class =>
            'background-image: linear-gradient({{' . $id . 'gradient_direction.VALUE}}, {{' . $id . 'gradient_color_1.VALUE}}, {{' . $id . 'gradient_color_2.VALUE}});',
        ],
      ]
    );

    $this->add_control(
      $id . 'gradient_direction',
      [
        'label' => __('Kierunek ' . $title, 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'to right' => esc_html__('W prawo', 'ecp-widget'),
          'to left' => esc_html__('W lewo', 'ecp-widget'),
          'to bottom' => esc_html__('W dół', 'ecp-widget'),
          'to top' => esc_html__('W górę', 'ecp-widget'),
        ],
        'default' => 'to right',
        'selectors' => [
          '{{WRAPPER}}' . $class => 'background-image: linear-gradient({{VALUE}}, {{' . $id . 'gradient_color_1.VALUE}}, {{' . $id . 'gradient_color_2.VALUE}});',
        ],
      ]
    );
  }

  protected function add_bg_options($id, $class)
  {
    $this->add_responsive_control(
      $id . 'position',
      [
        'label' => __('Background position', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'left top' => __('Left Top', 'ecp-widget'),
          'left center' => __('Left Center', 'ecp-widget'),
          'left bottom' => __('Left Bottom', 'ecp-widget'),
          'center top' => __('Center Top', 'ecp-widget'),
          'center center' => __('Center Center', 'ecp-widget'),
          'center bottom' => __('Center Bottom', 'ecp-widget'),
          'right top' => __('Right Top', 'ecp-widget'),
          'right center' => __('Right Center', 'ecp-widget'),
          'right bottom' => __('Right Bottom', 'ecp-widget'),
          'custom' => __('Custom', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'background-position: {{VALUE}};',
        ],

      ]
    );

    $this->add_group_control(
      Group_Control_Css_Filter::get_type(),
      [
        'name' => $id . 'filter',
        'label' => __('Background filter', 'ecp-widget'),
        'types' => ['classic', 'gradient'],
        'selector' => '{{WRAPPER}} ' . $class,
      ]
    );

    $this->add_responsive_control(
      $id . 'position-custom',
      [
        'label' => __('Custom position', 'ecp-widget'),
        'type' => Controls_Manager::TEXT,
        'condition' => [
          $id . 'position' => 'custom',
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'background-position: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . 'repeat',
      [
        'label' => __('Background repeat', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'repeat-x' => __('Repeat X', 'ecp-widget'),
          'repeat' => __('Repeat', 'ecp-widget'),
          'space' => __('Space', 'ecp-widget'),
          'round' => __('Round', 'ecp-widget'),
          'no-repeat' => __('No repeat', 'ecp-widget'),
          'space repeat' => __('Space repeat', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'background-repeat: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . 'size',
      [
        'label' => __('Background size', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'contain' => __('Contain', 'ecp-widget'),
          'cover' => __('Cover', 'ecp-widget'),
          'inherit' => __('Inherit', 'ecp-widget'),
          'initial' => __('Initial', 'ecp-widget'),
          'revert' => __('Revert', 'ecp-widget'),
          'revert-layer' => __('Revert layer', 'ecp-widget'),
          'unset' => __('Unset', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'background-size: {{VALUE}};',
        ],

      ]
    );
  }

  protected function add_width($id, $class, $title = 'Width')
  {
    $this->add_responsive_control(
      $id,
      [
        'type' => Controls_Manager::SLIDER,
        'label' => esc_html__($title, 'ecp-widget'),
        'size_units' => ['px', '%', 'vh', 'custom'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
            'step' => 1,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'width: {{SIZE}}{{UNIT}};',
        ],

      ]
    );
  }

  protected function add_height($id, $class, $title = 'Height')
  {
    $this->add_responsive_control(
      $id,
      [
        'type' => Controls_Manager::SLIDER,
        'label' => esc_html__($title, 'ecp-widget'),
        'size_units' => ['px', '%', 'vh', 'custom'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
            'step' => 1,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'height: {{SIZE}}{{UNIT}};',
        ],
      ]
    );
  }

  protected function add_aspect($id, $class, $title = 'Aspect ratio')
  {
    $this->add_responsive_control(
      $id,
      [
        'type' => Controls_Manager::TEXT,
        'label' => esc_html__($title, 'ecp-widget'),
        'description' => 'Aspect ratio = width / height',
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'aspect-ratio: {{SIZE}};',
        ],

      ]
    );
  }


  protected function add_max_height($id, $class, $title = 'Max height')
  {
    $this->add_responsive_control(
      $id,
      [
        'type' => Controls_Manager::SLIDER,
        'label' => esc_html__($title, 'ecp-widget'),
        'size_units' => ['px', '%', 'vh'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
            'step' => 1,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'max-height: {{SIZE}}{{UNIT}};',
        ],

      ]
    );
  }

  protected function add_min_width($id, $class, $title = 'Min width')
  {
    $this->add_responsive_control(
      $id,
      [
        'type' => Controls_Manager::SLIDER,
        'label' => esc_html__($title, 'ecp-widget'),
        'size_units' => ['px', '%', 'vh'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
            'step' => 1,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'min-width: {{SIZE}}{{UNIT}};',
        ],
      ]
    );
  }

  protected function add_max_width($id, $class, $title = 'Max width')
  {
    $this->add_responsive_control(
      $id,
      [
        'type' => Controls_Manager::SLIDER,
        'label' => esc_html__($title, 'ecp-widget'),
        'size_units' => ['px', '%', 'vh'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
            'step' => 1,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'max-width: {{SIZE}}{{UNIT}};',
        ],
      ]
    );
  }

  protected function add_opacity($id, $class)
  {
    $this->add_responsive_control(
      $id,
      [
        'type' => Controls_Manager::SLIDER,
        'label' => __('Opacity', 'ecp-widget'),
        'size_units' => ['%'],
        'range' => [
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'opacity: calc({{SIZE}}/100);',
        ],

      ]
    );
  }

  protected function add_gap($id, $class, $title = 'Gap')
  {
    $this->add_responsive_control(
      $id,
      [
        'type' => Controls_Manager::SLIDER,
        'label' => __($title, 'ecp-widget'),
        'size_units' => ['px', '%', 'em'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'gap: {{SIZE}}{{UNIT}};',
        ],

      ]
    );
  }

  protected function add_flex_pack($id, $class)
  {
    $this->add_responsive_control(
      $id . '_justify_content',
      [
        'label' => __('Justify content', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'flex-start' => __('Start', 'ecp-widget'),
          'flex-end' => __('End', 'ecp-widget'),
          'center' => __('Center', 'ecp-widget'),
          'space-between' => __('Space between', 'ecp-widget'),
          'space-around' => __('Space around', 'ecp-widget'),
          'space-evenly' => __('Space evenly', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'justify-content: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . '_align_items',
      [
        'label' => __('Align items', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'flex-start' => __('Start', 'ecp-widget'),
          'flex-end' => __('End', 'ecp-widget'),
          'center' => __('Center', 'ecp-widget'),
          'baseline' => __('Baseline', 'ecp-widget'),
          'stretch' => __('Stretch', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'align-items: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . '_flex_dierction',
      [
        'label' => __('Flex direction', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'row' => __('Row', 'ecp-widget'),
          'row-reverse' => __('Row reverse', 'ecp-widget'),
          'column' => __('Column', 'ecp-widget'),
          'column-reverse' => __('Column reverse', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'flex-direction: {{VALUE}};',
        ],

      ]
    );
  }

  protected function add_flex_child_pack($id, $class)
  {
    $this->add_responsive_control(
      $id . '_flex_grow',
      [
        'label' => __('Flex grow', 'ecp-widget'),
        'type' => Controls_Manager::NUMBER,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'flex-grow: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . '_flex_shrink',
      [
        'label' => __('Flex shrink', 'ecp-widget'),
        'type' => Controls_Manager::NUMBER,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'flex-shrink: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . '_flex_basis',
      [
        'label' => __('Flex basis', 'ecp-widget'),
        'type' => Controls_Manager::NUMBER,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'flex-basis: {{VALUE}}%;',
        ],

      ]
    );
  }

  protected function add_image_pack($id, $class)
  {
    $this->add_responsive_control(
      $id . '_obj_fit',
      [
        'label' => __('Object fit', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'fill' => __('Fill', 'ecp-widget'),
          'contain' => __('Contain', 'ecp-widget'),
          'cover' => __('Cover', 'ecp-widget'),
          'none' => __('None', 'ecp-widget'),
          'scale-down' => __('Scale down', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'object-fit: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . '_obj_position',
      [
        'label' => __('Object position', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'left top' => __('Left Top', 'ecp-widget'),
          'left center' => __('Left Center', 'ecp-widget'),
          'left bottom' => __('Left Bottom', 'ecp-widget'),
          'center top' => __('Center Top', 'ecp-widget'),
          'center center' => __('Center Center', 'ecp-widget'),
          'center bottom' => __('Center Bottom', 'ecp-widget'),
          'right top' => __('Right Top', 'ecp-widget'),
          'right center' => __('Right Center', 'ecp-widget'),
          'right bottom' => __('Right Bottom', 'ecp-widget'),
          'custom' => __('Custom', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'object-position: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . '_obj_position_custom',
      [
        'label' => __('Custom position', 'ecp-widget'),
        'type' => Controls_Manager::TEXT,
        'condition' => [
          $id . '_obj_position' => 'custom',
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'background-position: {{VALUE}};',
        ],

      ]
    );
  }

  protected function add_icon_pack($id, $class, $title = 'Icon')
  {
    $this->add_responsive_control(
      $id . '_size',
      [
        'label' => __($title . ' size', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em'],
        'default' => [
          'size' => 16,
          'unit' => 'px',
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'width: {{SIZE}}{{UNIT}};',
          '{{WRAPPER}} ' . $class . ' svg[aria-hidden="true"]' => 'width: {{SIZE}}{{UNIT}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . '_fill',
      [
        'label' => __($title . ' fill', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'fill: {{VALUE}};',
          '{{WRAPPER}} ' . $class . ' svg' => 'fill: {{VALUE}};',
          '{{WRAPPER}} ' . $class . ' svg path' => 'fill: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . '_stroke',
      [
        'label' => __($title . ' stroke', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'stroke: {{VALUE}};',
          '{{WRAPPER}} ' . $class . ' svg' => 'stroke: {{VALUE}};',
        ],

      ]
    );

    $this->add_responsive_control(
      $id . '_stroke_width',
      [
        'label' => __($title . ' stroke width', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'stroke-width: {{VALUE}};',
          '{{WRAPPER}} ' . $class . ' svg' => 'stroke-width: {{VALUE}};',
        ],

      ]
    );
  }

  protected function add_transform($id, $class, $title = 'Transform') {
    $this->add_responsive_control(
      $id . '_rotate',
      [
        'label' => __($title . ' rotate', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'min' => -360,
        'max' => 360,
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'transform: rotate({{SIZE}}deg);',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_scale',
      [
        'label' => __($title . ' scale', 'ecp-widget'),
        'type' => Controls_Manager::NUMBER,
        'size_units' => [''],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'transform: scale({{SIZE}});',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_translate',
      [
        'label' => __($title . ' translate', 'ecp-widget'),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%', 'em'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'transform: translate({{LEFT}}{{UNIT}}, {{TOP}}{{UNIT}});',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_skew',
      [
        'label' => __($title . ' skew', 'ecp-widget'),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['deg'],
        'selectors' => [
          '{{WRAPPER}} ' . $class => 'transform: skew({{LEFT}}{{UNIT}}, {{TOP}}{{UNIT}});',
        ],
      ]
    );
  }
}