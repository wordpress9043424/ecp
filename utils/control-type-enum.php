<?php
namespace ECP\Enum;
if ( ! defined( 'ABSPATH' ) ) exit;
enum Control_Type: string {
  case URL = 'url';
  case ICON = 'icon';
  case SELECT = 'select';
  case EDITOR = 'editor';
  case SWITCHER = 'switcher';
  case TEXT = 'text';
  case NUMBER = 'number';
  case TEMPLATE = 'template';
  case MEDIA = 'media';
  case REPEATER = 'repeater';

  case TABS = 'tabs';
  case VARIABLE = 'variable';
  case DIVIDER = 'divider';
  
  case SIZE = 'size';
  case GRID = 'grid';
  case BORDER = 'border';
  case FLEXBOX = 'flexbox';
  case SPACING = 'spacing';
  case POSITION = 'position';
  case TRANSFORM = 'transform';
  case ICON_STYLE = 'icon_style';
  case TYPOGRAPHY = 'typography';
  case BACKGROUND = 'background';
  case BACKGROUND_COLOR = 'background_color';
  case COMPOSITION = 'composition';
  case IMAGE_STYLE = 'image_style';
  case FLEXBOX_ITEM = 'flexbox_item';
}
