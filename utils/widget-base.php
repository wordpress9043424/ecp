<?php
namespace ECP\Utils;

use Elementor\Plugin;
use Elementor\Repeater;
use ECP\Enum\Control_Type;
use Elementor\Widget_Base;
use Elementor\Icons_Manager;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Text_Stroke;
use Elementor\Group_Control_Grid_Container;
use Elementor\Group_Control_Flex_Container;

abstract class ECP_Widget_Base extends Widget_Base
{
  private $devices = ['mobile', 'mobile_extra', 'tablet', 'tablet_extra', 'laptop', 'widescreen'];
  protected function register_controls(): void
  {
    $this->add_content_controls(sections: $this->content_controls());
    $this->add_style_controls(sections: $this->style_controls());
  }

  public function getId(): string {
    return $this->get_name() . $this->get_id();
  }

  protected function addJsConfig($where, $data): void
  {
    wp_localize_script($where, $this->getId(), $data);
  }

  protected function switch_content($control, $obj = null): void
  {
    $obj ??= $this;
    switch ($control['type']) {
      case Control_Type::URL:
        $this->add_url($control, $obj);
        break;
      case Control_Type::ICON:
        $this->add_icon($control, $obj);
        break;
      case Control_Type::SELECT:
        $this->add_select($control, $obj);
        break;
      case Control_Type::EDITOR:
        $this->add_editor($control, $obj);
        break;
      case Control_Type::SWITCHER:
        $this->add_switcher($control, $obj);
        break;
      case Control_Type::TEXT:
        $this->add_text($control, $obj);
        break;
      case Control_Type::NUMBER:
        $this->add_number($control, $obj);
        break;
      case Control_Type::MEDIA:
        $this->add_image($control, $obj);
        break;
      case Control_Type::TEMPLATE:
        $this->add_template($control, $obj);
        break;
      case Control_Type::DIVIDER:
        $this->add_divider($obj);
        break;
    }
  }

  protected function switch_style($control): void
  {
    switch ($control['type']) {
      case Control_Type::BORDER:
        $this->add_border($control);
        break;
      case Control_Type::FLEXBOX:
        $this->add_flexbox($control);
        break;
      case Control_Type::SPACING:
        $this->add_spacing($control);
        break;
      case Control_Type::TYPOGRAPHY:
        $this->add_typography($control);
        break;
      case Control_Type::BACKGROUND:
        $this->add_background($control);
        break;
      case Control_Type::ICON_STYLE:
        $this->add_icon_style($control);
        break;
      case Control_Type::TRANSFORM:
        $this->add_transform($control);
        break;
      case Control_Type::SIZE:
        $this->add_size($control);
        break;
      case Control_Type::BACKGROUND_COLOR:
        $this->add_bg_color($control);
        break;
      case Control_Type::POSITION:
        $this->addPosition($control);
        break;
      case Control_Type::IMAGE_STYLE:
        $this->add_image_style($control);
        break;
      case Control_Type::DIVIDER:
        $this->add_divider();
        break;
      case Control_Type::GRID:
        $this->add_grid($control);
        break;
      case Control_Type::COMPOSITION:
        $this->add_composition($control);
        break;
      default:
        $this->add_default($control);
        break;
    }
  }
  
  protected function add_default($control): void {
    $id = 'ecp_' . $control['id'];
    unset($control['id']);
    $this->add_control(
      $id,
      [
        ...$control,
        'label' => __($control['label'], 'ecp-widget'),
      ]
    );
  }

  protected function add_content_controls($sections): void
  {
    foreach ($sections as $section) {
      $this->start_controls_section(
        'ecp_' . $section['id'],
        [
          'label' => __($section['label'], 'ecp-widget'),
          'tab' => Controls_Manager::TAB_CONTENT,
        ]
      );

      foreach ($section['controls'] as $control) {
        if ($control['type'] == Control_Type::REPEATER)
          $this->add_repeater($control, $this);
        else
          $this->switch_content($control);
      }

      $this->end_controls_section();
    }
  }

  protected function add_style_controls($sections): void
  {
    foreach ($sections as $section) {
      $this->start_controls_section(
        'ecp_' . $section['id'],
        [
          'label' => __($section['label'], 'ecp-widget'),
          'tab' => Controls_Manager::TAB_STYLE,
        ]
      );
      if (!empty($section['controls'])) {
        foreach ($section['controls'] as $control) {
          $element = [
            'class' => $section['class'],
            'id' => $section['id'],
            ...$control
          ];
          if ($control['type'] == Control_Type::TABS) {
            $this->addTabs($element);
            continue;
          }
          $this->switch_style($element);
        }
      }

      $this->end_controls_section();
    }
  }

  protected function add_divider($obj = null): void
  {
    $obj ??= $this;
    $obj->add_control(
      uniqid('ecp_divider_'),
      [
        'type' => Controls_Manager::DIVIDER,
        'style' => 'thick',
      ]
    );
  }

  protected function add_url($control, $obj): void
  {
    $obj->add_control(
      'ecp_' . $control['id'],
      [
        ...$control,
        'label' => __($control['label'], 'ecp-widget'),
        'type' => Controls_Manager::URL,
        'dynamic' => [
          'active' => true,
        ],
        'autocomplete' => true,
        'label_block' => true,
        'placeholder' => __('Enter your link', 'ecp-widget'),
        'default' => [
          'is_external' => true,
        ],
      ]
    );
  }

  protected function add_image($control, $obj): void
  {
    $obj->add_control(
      'ecp_' . $control['id'],
      [
        ...$control,
        'label' => __($control['label'], 'ecp-widget'),
        'type' => Controls_Manager::MEDIA,
      ]
    );
  }

  protected function add_icon($control, $obj): void
  {
    $obj->add_control(
      'ecp_' . $control['id'],
      [
        ...$control,
        'label' => __($control['label'], 'ecp-widget'),
        'type' => Controls_Manager::ICONS,
        'fa4compatibility' => 'icon',
        'skin' => 'inline',
        'label_block' => false,
      ]
    );
  }

  protected function add_number($control, $obj): void
  {
    $id = 'ecp_' . $control['id'];
    if ($this->isResponsive($control)) {
      foreach($this->devices as $device) {
        $obj->add_control(
          $id . '_' . $device,
          [
            ...$control,
            'type' => Controls_Manager::NUMBER,
            'devices' => [ $device ],
          ]
        );
      }
      return;
    }
    $obj->add_control(
      $id,
      [
        ...$control,
        'label' => __($control['label'], 'ecp-widget'),
        'type' => Controls_Manager::NUMBER,
      ]
    );
  }

  protected function add_text($control, $obj): void
  {
    $id = 'ecp_' . $control['id'];
    if ($this->isResponsive($control)) {
      foreach($this->devices as $device) {
        $obj->add_control(
          $id . '_' . $device,
          [
            ...$control,
            'type' => Controls_Manager::TEXT,
            'devices' => [ $device ],
          ]
        );
      }
      return;
    }
    $obj->add_control(
      $id,
      [
        ...$control,
        'label' => __($control['label'], 'ecp-widget'),
        'type' => Controls_Manager::TEXT,
      ]
    );
  }

  protected function add_select($control, $obj): void
  {
    $id = 'ecp_' . $control['id'];
    if ($this->isResponsive($control)) {
        foreach($this->devices as $device) {
          $obj->add_control(
            $id . '_' . $device,
            [
              ...$control,
              'label' => __($control['label'], 'ecp-widget'),
              'type' => Controls_Manager::SELECT,
              'devices' => [ $device ],
            ]
          );
        }
        return;
    }
    $obj->add_control(
      $id,
      [
        ...$control,
        'label' => __($control['label'], 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
      ]
    );
  }

  protected function add_editor($control, $obj): void
  {
    $obj->add_control(
      'ecp_' . $control['id'],
      [
        ...$control,
        'label' => __($control['label'], 'ecp-widget'),
        'type' => Controls_Manager::WYSIWYG,
        'separator' => 'before',
      ]
    );
  }

  protected function add_switcher($control, $obj): void
  {
    $obj->add_control(
      'ecp_' . $control['id'],
      [
        ...$control,
        'label' => __($control['label'], 'ecp-widget'),
        'type' => Controls_Manager::SWITCHER,
        'label_on' => true,
        'label_off' => false,
      ]
    );
  }

  protected function add_template($control, $obj): void
  {
    $obj->add_control(
      'ecp_' . $control['id'],
      [
        'label' => __($control['label'], 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'default' => '0',
        'options' => $this->getAllTemplates(),
      ]
    );
  }

  protected function add_repeater($control, $obj): void
  {
    $repeater = new Repeater();
    foreach ($control['controls'] as $repeater_control) {
      $this->switch_content($repeater_control, $repeater);
    }

    $obj->add_control(
      'ecp_' . $control['id'],
      array_merge(
        $control,
        [
          'type' => Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
        ]
      )
    );
  }

  protected function add_border($control): void
  {
    $id = 'ecp_' . $control['id'] . '_border';
    unset($control['id']);
    $this->add_responsive_control(
      $id . '_radius',
      [
        'label' => __('Border radius', 'ecp-widget'),
        ...$control,
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%'],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );

    $this->add_group_control(
      Group_Control_Border::get_type(),
      [
        'label' => __('Border', 'ecp-widget'),
        ...$control,
        'name' => $id,
        'selector' => '{{WRAPPER}} ' . $control['class'],
      ]
    );

    $this->add_group_control(
      Group_Control_Box_Shadow::get_type(),
      [
        'label' => __('Box shadow', 'ecp-widget'),
        ...$control,
        'name' => $id . '_box_shadow',
        'selector' => '{{WRAPPER}} ' . $control['class'],
      ]
    );
  }

  protected function add_flexbox($control): void
  {
    $id = 'ecp_' . $control['id'] . '_flexbox';
    unset($control['id']);
    $this->add_group_control(
      Group_Control_Flex_Container::get_type(),
      [
        'name' => $id,
        ...$control,
        'selector' => '{{WRAPPER}} ' . $control['class']
      ]
    );
  }
  
  protected function add_grid($control): void
  {
    $id = 'ecp_' . $control['id'] . '_grid';
    unset($control['id']);
    $this->add_group_control(
      Group_Control_Grid_Container::get_type(),
      [
        'name' => $id,
        ...$control,
        'selector' => '{{WRAPPER}} ' . $control['class']
      ]
    );
  }
  
  protected function add_composition($control): void
  {
    $id = 'ecp_' . $control['id'];
    unset($control['id']);
    $this->add_control(
      $id . '_select',
      [
        'label' => __('Select composition', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'flex' => __('Flex', 'ecp-widget'),
          'grid' => __('Grid', 'ecp-widget'),
        ],
        'default' => 'flex',
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'display: {{VALUE}};',
        ],
      ]
    );
    $this->add_group_control(
      Group_Control_Grid_Container::get_type(),
      [
        'name' => $id . '_grid',
        ...$control,
        'selector' => '{{WRAPPER}} ' . $control['class'],
        'condition' => [
          $id . '_select' => 'grid',
        ]
      ]
    );
    $this->add_group_control(
      Group_Control_Flex_Container::get_type(),
      [
        'name' => $id . '_flex',
        ...$control,
        'selector' => '{{WRAPPER}} ' . $control['class'],
        'condition' => [
          $id . '_select' => 'flex',
        ]
      ]
    );
  }
  
  protected function add_typography($control): void
  {
    $id = 'ecp_' . $control['id'] . '_typography';
    unset($control['id']);
    $this->add_responsive_control(
      $id . '_color',
      [
        'label' => __('Text color', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'color: {{VALUE}};',
        ],

      ]
    );
    $this->add_responsive_control(
      $id . '_white_space',
      [
        'label' => __('White space'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'normal' => __('Normal', 'ecp-widget'),
          'nowrap' => __('No wrap', 'ecp-widget'),
          'pre' => __('Pre', 'ecp-widget'),
          'pre-wrap' => __('Pre wrap', 'ecp-widget'),
          'pre-line' => __('Pre line', 'ecp-widget'),
          'break-spaces' => __('Break spaces', 'ecp-widget'),
        ],
        'default' => 'normal',
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'white-space: {{VALUE}}'
        ]
      ]
    );
    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => $id . '_typography',
        'label' => __('Typography', 'ecp-widget'),
        'selector' => '{{WRAPPER}} ' . $control['class'],
      ]
    );
    $this->add_group_control(
      Group_Control_Text_Shadow::get_type(),
      [
        'name' => $id . '_text_shadow',
        'label' => __('Text shadow', 'ecp-widget'),
        'selector' => '{{WRAPPER}} ' . $control['class'],
      ]
    );
    $this->add_group_control(
      Group_Control_Text_Stroke::get_type(),
      [
        'name' => $id . '_text_stroke',
        'label' => __('Text stroke', 'ecp-widget'),
        'selector' => '{{WRAPPER}} ' . $control['class'],
      ]
    );
  }

  protected function add_spacing($control): void
  {
    $id = 'ecp_' . $control['id'] . '_spacing';
    unset($control['id']);
    $this->add_responsive_control(
      $id . '_margin',
      [
        'label' => __('Margin', 'ecp-widget'),
        ...$control,
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%', 'em', 'custom'],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );
    $this->add_responsive_control(
      $id . '_padding',
      [
        'label' => __('Padding', 'ecp-widget'),
        ...$control,
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%', 'em', 'custom'],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );
  }

  protected function add_transform($control): void
  {
    $id = 'ecp_' . $control['id'] . '_transform';
    unset($control['id']);
    $this->add_responsive_control(
      $id . '_translate_x',
      [
        'label' => __('Translate X', 'ecp-widget'),
        ...$control,
        'type' => Controls_Manager::SLIDER,
        'size_units' => [ 'px', '%' ],
        'range' => [
          'px' => [
            'min' => -500,
            'max' => 500,
          ],
          '%' => [
            'min' => -200,
            'max' => 200,
          ],
        ],
        'default' => [
          'unit' => 'px',
          'size' => 0,
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => '--transform-translate-x: {{SIZE}}{{UNIT}};',
        ],
      ]
    );
    $this->add_responsive_control(
      $id . '_translate_y',
      [
        'label' => __('Translate Y', 'ecp-widget'),
        ...$control,
        'type' => Controls_Manager::SLIDER,
        'size_units' => [ 'px', '%' ],
        'range' => [
          'px' => [
            'min' => -500,
            'max' => 500,
          ],
          '%' => [
            'min' => -200,
            'max' => 200,
          ],
        ],
        'default' => [
          'unit' => 'px',
          'size' => 0,
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => '--transform-translate-y: {{SIZE}}{{UNIT}};',
        ],
      ]
    );
    $this->add_responsive_control(
      $id . '_rotate',
      [
        'label' => __('Rotate', 'ecp-widget'),
        ...$control,
        'type' => Controls_Manager::SLIDER,
        'size_units' => [ 'deg' ],
        'range' => [
          'deg' => [
            'min' => -360,
            'max' => 360,
          ],
        ],
        'default' => [
          'unit' => 'deg',
          'size' => 0,
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => '--transform-rotate: {{SIZE}}{{UNIT}};',
        ],
      ]
    );
  }

  protected function add_icon_style($control): void
  {
    $id = 'ecp_' . $control['id'] . '_icon';
    unset($control['id']);
    
    $this->add_responsive_control(
      $id . '_size',
      [
        'label' => __('Icon size', 'ecp-widget'),
        ...$control,
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%', 'em', 'custom'],
        'default' => [
          'size' => 16,
          'unit' => 'px',
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] . ' svg[aria-hidden="true"]' => 'width: {{SIZE}}{{UNIT}};',
        ],
      ]
    );
    $this->add_responsive_control(
      $id . '_fill',
      [
        'label' => __('Icon fill', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] . ' svg' => 'fill: {{VALUE}};',
          '{{WRAPPER}} ' . $control['class'] . ' svg path' => 'fill: {{VALUE}};',
        ],

      ]
    );
    $this->add_responsive_control(
      $id . '_stroke',
      [
        'label' => __('Icon stroke', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] . ' svg' => 'stroke: {{VALUE}};',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_stroke_width',
      [
        'label' => __('Icon stroke width', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'stroke-width: {{VALUE}};',
          '{{WRAPPER}} ' . $control['class'] . ' svg' => 'stroke-width: {{VALUE}};',
        ],
      ]
    );
  }
  
  protected function add_bg_color($control): void
  {
    $id = 'ecp_' . $control['id'] . '_icon';
    unset($control['id']);
    
    $this->add_responsive_control(
      $id . '_bg_color',
      [
        'label' => __('Background color', 'ecp-widget'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'background-color: {{VALUE}};',
        ],
      
      ]
    );
  }
  
  protected function add_size($control): void
  {
    $id = 'ecp_' . $control['id'] . '_size';
    unset($control['id']);
    $this->add_control(
      $id . '_width',
      [
        'label' => __('Width', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vw'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vw' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'width: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      $id . '_min_width',
      [
        'label' => __('Min Width', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vw'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vw' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'min-width: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      $id . '_max_width',
      [
        'label' => __('Max Width', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vw'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vw' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'max-width: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      $id . '_height',
      [
        'label' => __('Height', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vh'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'height: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      $id . '_min_height',
      [
        'label' => __('Min Height', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vh'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'min-height: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      $id . '_max_height',
      [
        'label' => __('Max Height', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vh'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'max-height: {{SIZE}}{{UNIT}};',
        ],
      ]
    );

    $this->add_control(
      $id . '_aspect_ratio',
      [
        'label' => __('Aspect ratio', 'ecp-widget'),
        'type' => Controls_Manager::TEXT,
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'aspect-ratio: {{VALUE}};',
        ],
      ]
      );
  }

  protected function add_background($control): void
  {
    $id = 'ecp_' . $control['id'] . '_background';
    unset($control['id']);
    $this->add_group_control(
      Group_Control_Background::get_type(),
      [
        ...$control,
        'name' => $id,
        'types' => ['classic', 'gradient', 'video', 'image'],
        'selector' => '{{WRAPPER}} ' . $control['class'],
      ]
    );
  }

  protected function add_image_style($control): void
  {
    $id = 'ecp_' . $control['id'] . '_image_style';
    unset($control['id']);
    $this->add_control(
      $id . '_object_fit',
      [
        'label' => __('Object Fit', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'fill' => __('Fill', 'ecp-widget'),
          'contain' => __('Contain', 'ecp-widget'),
          'cover' => __('Cover', 'ecp-widget'),
          'none' => __('None', 'ecp-widget'),
          'scale-down' => __('Scale Down', 'ecp-widget'),
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'object-fit: {{VALUE}};',
        ],
      ]
    );

    $this->add_control(
      $id . '_object_position',
      [
        'label' => __('Object Position', 'ecp-widget'),
        'type' => Controls_Manager::TEXT,
        'default' => 'center center',
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'object-position: {{VALUE}};',
        ],
      ]
    );

  }

  protected function addPosition($control): void
  {
    $id = 'ecp_' . $control['id'] . '_position';
    unset($control['id']);
    $this->add_responsive_control(
      $id,
      [
        'label' => __('Position', 'ecp-widget'),
        'type' => Controls_Manager::SELECT,
        'options' => [
          'static' => __('Static', 'ecp-widget'),
          'relative' => __('Relative', 'ecp-widget'),
          'absolute' => __('Absolute', 'ecp-widget'),
          'fixed' => __('Fixed', 'ecp-widget'),
          'sticky' => __('Sticky', 'ecp-widget'),
        ],
        'default' => 'static',
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'position: {{VALUE}};',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_top',
      [
        'label' => __('Top', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vh', 'vw'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'top: {{SIZE}}{{UNIT}};',
        ],
        'condition' => [
          'ecp_' . $control['id'] . '_position!' => 'static',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_left',
      [
        'label' => __('Left', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vh', 'vw'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'left: {{SIZE}}{{UNIT}};',
        ],
        'condition' => [
          'ecp_' . $control['id'] . '_position!' => 'static',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_right',
      [
        'label' => __('Right', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vh', 'vw'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'right: {{SIZE}}{{UNIT}};',
        ],
        'condition' => [
          'ecp_' . $control['id'] . '_position!' => 'static',
        ],
      ]
    );

    $this->add_responsive_control(
      $id . '_bottom',
      [
        'label' => __('Bottom', 'ecp-widget'),
        'type' => Controls_Manager::SLIDER,
        'size_units' => ['px', '%', 'em', 'rem', 'vh', 'vw'],
        'range' => [
          'px' => [
            'min' => 0,
            'max' => 2000,
          ],
          '%' => [
            'min' => 0,
            'max' => 100,
          ],
          'em' => [
            'min' => 0,
            'max' => 100,
          ],
          'rem' => [
            'min' => 0,
            'max' => 100,
          ],
          'vh' => [
            'min' => 0,
            'max' => 100,
          ],
        ],
        'selectors' => [
          '{{WRAPPER}} ' . $control['class'] => 'bottom: {{SIZE}}{{UNIT}};',
        ],
        'condition' => [
          'ecp_' . $control['id'] . '_position!' => 'static',
        ],
      ]
    );
  }

  protected function addTabs($control): void
  {
    $this->start_controls_tabs('ecp_' . $control['id']);
    if (!empty($control['tabs'])) {
      foreach ($control['tabs'] as $tab) {
        $this->start_controls_tab(
          'ecp_' . $tab['id'],
          [
            'label' => __($tab['label'], 'ecp-widget')
          ]
        );
        foreach ($control['controls'] as $tab_controls)
          $this->switch_style([
            'class' => $tab['class'],
            ...$tab_controls,
            'id' => $tab['id'],
          ]);
        $this->end_controls_tab();
      }
    }
    $this->end_controls_tabs();
  }

  protected function isResponsive($control): bool {
      return !empty($control['responsive']) && $control['responsive'];
  }
  
  protected function getResponsiveValue($obj, $key): array {
    $breakpoints = Plugin::$instance->breakpoints->get_breakpoints();
    $tab = [];
    foreach ( $breakpoints as $breakpointKey => $breakpoint ) {
      $tempKey = $key . '_' . $breakpointKey;
      if(!empty($obj[$tempKey]))
        $tab[$breakpoint->get_value()] = $obj[$tempKey];
    }
    return array_unique($tab);
  }

  protected function getAllTemplates(): array
  {
    $templates = Plugin::instance()->templates_manager->get_source('local')->get_items();
    if (empty($templates)) {
      return ['0' => 'You haven’t saved templates yet.'];
    }
    $template_options = ['0' => 'Select Template'];
    foreach ($templates as $template) {
      $template_options[$template['template_id']] = $template['title'];
    }

    return $template_options;
  }

  protected function getPostTypes(): array {
    $post_types = get_post_types(['public' => true], 'objects');
    $options = [];
    foreach ($post_types as $post_type) {
      $options[$post_type->name] = $post_type->label;
    }
    return $options;
  }

  protected function getLangPicker(): array
  {
    $array = ['none' => 'None'];
    if (function_exists('pll_current_language')) {
      $array['polylang'] = 'PolyLang';
    }
    return $array;
  }

  protected function getAllBreakpoints(): array
  {
      $breakpoints = Plugin::$instance->breakpoints->get_breakpoints();
      $breakpoints_array = [];
      foreach ( $breakpoints as $key => $breakpoint ) {
          $breakpoints_array[ $key ] = $breakpoint->get_value();
      }
      return $breakpoints_array;
  }
}
